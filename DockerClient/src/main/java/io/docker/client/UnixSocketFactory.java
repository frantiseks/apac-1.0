package io.docker.client;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import org.apache.http.HttpHost;
import org.apache.http.annotation.Immutable;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.protocol.HttpContext;
import org.newsclub.net.unix.AFUNIXSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author expi
 */
@Immutable
public class UnixSocketFactory implements ConnectionSocketFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnixSocketFactory.class);
    private static final int SOCKET_TIMEOUT = 10000;
    private final File socketFile;

    public UnixSocketFactory(File socketFile) {
        this.socketFile = socketFile;
    }

    @Override
    public Socket createSocket(HttpContext hc) throws IOException {
        return new UnixSocket();
    }

    @Override
    public Socket connectSocket(int i, Socket socket, HttpHost hh, InetSocketAddress isa, InetSocketAddress isa1, HttpContext hc) throws IOException {
        try {
            socket.connect(new AFUNIXSocketAddress(socketFile), SOCKET_TIMEOUT);
        } catch (SocketTimeoutException e) {
            LOGGER.error("Connect Socket ex", e);
        }
        return socket;
    }
}
