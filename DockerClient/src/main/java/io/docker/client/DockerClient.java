/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.client;

import io.docker.DockerException;
import io.docker.DockerNotFoundException;
import io.docker.DockerNotRunningException;
import io.docker.entity.Container;
import io.docker.entity.ContainerConfig;
import io.docker.entity.ContainerDetail;
import io.docker.entity.ContainerInfo;
import io.docker.entity.ContainerProcesses;
import io.docker.entity.DockerInfo;
import io.docker.entity.DockerVersion;
import java.util.List;

/**
 *
 * @author Frantisek Spacek
 */
public interface DockerClient {

    /**
     * get list of containers
     * @param all - include not running containers
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public List<Container> getContainers(boolean all)  throws DockerException, DockerNotRunningException;

    /**
     *
     * @param all - include not running containers
     * @param latest
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public List<Container> getContainers(boolean all, boolean latest)   throws DockerException, DockerNotRunningException;

    /**
     *
     * @param all
     * @param latest
     * @param limit
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public List<Container> getContainers(boolean all, boolean latest, int limit)  throws DockerException, DockerNotRunningException;

    /**
     *
     * @param all
     * @param latest
     * @param limit
     * @param showSize
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public List<Container> getContainers(boolean all, boolean latest, int limit, boolean showSize)  throws DockerException, DockerNotRunningException;

    /**
     *
     * @param all
     * @param latest
     * @param limit
     * @param showSize
     * @param since
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public List<Container> getContainers(boolean all, boolean latest, int limit, boolean showSize, String since)   throws DockerException, DockerNotRunningException;

    /**
     *
     * @param all
     * @param latest
     * @param limit
     * @param showSize
     * @param since
     * @param before
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public List<Container> getContainers(boolean all, boolean latest, int limit, boolean showSize, String since, String before)   throws DockerException, DockerNotRunningException;

    /**
     * @param id
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public ContainerDetail inspectContainer(String id)   throws DockerException, DockerNotRunningException, DockerNotFoundException;

    /**
     *
     * @param config
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public ContainerInfo createContainer(ContainerConfig config)   throws DockerException, DockerNotRunningException, DockerNotFoundException;

    /**
     *
     * @param id
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public void startContainer(String id)   throws DockerException, DockerNotRunningException, DockerNotFoundException;

    /**
     *
     * @param id
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public void restartContainer(String id)   throws DockerException, DockerNotRunningException, DockerNotFoundException;

     /**
     *
     * @param id
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public void killContainer(String id)   throws DockerException, DockerNotRunningException, DockerNotFoundException;

     /**
     *
     * @param id
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public void stopContainer(String id)   throws DockerException, DockerNotRunningException, DockerNotFoundException;

   /**
     *
     * @param id
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     * @throws io.docker.DockerNotFoundException
     */
    public void deleteContainer(String id)   throws DockerException, DockerNotRunningException, DockerNotFoundException;
    
    /**
     * 
     * @return
     * @throws DockerException 
     * @throws DockerNotRunningException 
     */
    public DockerVersion getVersion()   throws DockerException, DockerNotRunningException;

    /**
     * 
     * @return
     * @throws io.docker.DockerException
     * @throws io.docker.DockerNotRunningException
     */
    public DockerInfo getInfo()   throws DockerException, DockerNotRunningException;
    
    /**
     * 
     * @param id
     * @param psArgs arguments for ps command
     * @return
     * @throws DockerException
     * @throws DockerNotRunningException
     * @throws DockerNotFoundException 
     */
    public ContainerProcesses getContainerProcesses(String id, String psArgs)  throws DockerException, DockerNotRunningException, DockerNotFoundException;
}
