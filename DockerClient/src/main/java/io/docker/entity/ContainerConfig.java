/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.entity;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Frantisek Spacek
 */
public class ContainerConfig {

    @SerializedName("Hostname")
    private String hostName;

    @SerializedName("PortSpecs")
    private String[] portSpecs;

    @SerializedName("User")
    private String user;

    @SerializedName("Tty")
    private boolean tty;

    @SerializedName("OpenStdin")
    private boolean stdinOpen;

    @SerializedName("StdinOnce")
    private boolean stdInOnce;

    @SerializedName("Memory")
    private long memoryLimit = 0;

    @SerializedName("MemorySwap")
    private long memorySwap = 0;

    @SerializedName("CpuShares")
    private int cpuShares = 0;

    @SerializedName("AttachStdin")
    private boolean attachStdin = false;

    @SerializedName("AttachStdout")
    private boolean attachStdout = false;

    @SerializedName("AttachStderr")
    private boolean attachStderr = false;

    @SerializedName("Env")
    private String[] env;

    @SerializedName("Cmd")
    private String[] cmd;

    @SerializedName("Dns")
    private String[] dns;

    @SerializedName("Image")
    private String image;

    @SerializedName("Volumes")
    private Object volumes;

    @SerializedName("VolumesFrom")
    private String volumesFrom = "";

    @SerializedName("Entrypoint")
    private String[] entrypoint = new String[]{};

    @SerializedName("NetworkDisabled")
    private boolean networkDisabled = false;

    @SerializedName("Privileged")
    private boolean privileged = false;

    @SerializedName("WorkingDir")
    private String workingDir = "";

    @SerializedName("Domainname")
    public String domainName;

    @SerializedName("ExposedPorts")
    public Object exposedPorts;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String[] getPortSpecs() {
        return portSpecs;
    }

    public void setPortSpecs(String[] portSpecs) {
        this.portSpecs = portSpecs;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isTty() {
        return tty;
    }

    public void setTty(boolean tty) {
        this.tty = tty;
    }

    public boolean isStdinOpen() {
        return stdinOpen;
    }

    public void setStdinOpen(boolean stdinOpen) {
        this.stdinOpen = stdinOpen;
    }

    public boolean isStdInOnce() {
        return stdInOnce;
    }

    public void setStdInOnce(boolean stdInOnce) {
        this.stdInOnce = stdInOnce;
    }

    public long getMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(long memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    public long getMemorySwap() {
        return memorySwap;
    }

    public void setMemorySwap(long memorySwap) {
        this.memorySwap = memorySwap;
    }

    public int getCpuShares() {
        return cpuShares;
    }

    public void setCpuShares(int cpuShares) {
        this.cpuShares = cpuShares;
    }

    public boolean isAttachStdin() {
        return attachStdin;
    }

    public void setAttachStdin(boolean attachStdin) {
        this.attachStdin = attachStdin;
    }

    public boolean isAttachStdout() {
        return attachStdout;
    }

    public void setAttachStdout(boolean attachStdout) {
        this.attachStdout = attachStdout;
    }

    public boolean isAttachStderr() {
        return attachStderr;
    }

    public void setAttachStderr(boolean attachStderr) {
        this.attachStderr = attachStderr;
    }

    public String[] getEnv() {
        return env;
    }

    public void setEnv(String[] env) {
        this.env = env;
    }

    public String[] getCmd() {
        return cmd;
    }

    public void setCmd(String[] cmd) {
        this.cmd = cmd;
    }

    public String[] getDns() {
        return dns;
    }

    public void setDns(String[] dns) {
        this.dns = dns;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Object getVolumes() {
        return volumes;
    }

    public void setVolumes(Object volumes) {
        this.volumes = volumes;
    }

    public String getVolumesFrom() {
        return volumesFrom;
    }

    public void setVolumesFrom(String volumesFrom) {
        this.volumesFrom = volumesFrom;
    }

    public String[] getEntrypoint() {
        return entrypoint;
    }

    public void setEntrypoint(String[] entrypoint) {
        this.entrypoint = entrypoint;
    }

    public boolean isNetworkDisabled() {
        return networkDisabled;
    }

    public void setNetworkDisabled(boolean networkDisabled) {
        this.networkDisabled = networkDisabled;
    }

    public boolean isPrivileged() {
        return privileged;
    }

    public void setPrivileged(boolean privileged) {
        this.privileged = privileged;
    }

    public String getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(String workingDir) {
        this.workingDir = workingDir;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Object getExposedPorts() {
        return exposedPorts;
    }

    public void setExposedPorts(Object exposedPorts) {
        this.exposedPorts = exposedPorts;
    }

    @Override
    public String toString() {
        return "ContainerConfig{" + "hostName=" + hostName + ", portSpecs=" + portSpecs + ", user=" + user + ", tty=" + tty + ", stdinOpen=" + stdinOpen + ", stdInOnce=" + stdInOnce + ", memoryLimit=" + memoryLimit + ", memorySwap=" + memorySwap + ", cpuShares=" + cpuShares + ", attachStdin=" + attachStdin + ", attachStdout=" + attachStdout + ", attachStderr=" + attachStderr + ", env=" + env + ", cmd=" + cmd + ", dns=" + dns + ", image=" + image + ", volumes=" + volumes + ", volumesFrom=" + volumesFrom + ", entrypoint=" + entrypoint + ", networkDisabled=" + networkDisabled + ", privileged=" + privileged + ", workingDir=" + workingDir + ", domainName=" + domainName + ", exposedPorts=" + exposedPorts + '}';
    }
}
