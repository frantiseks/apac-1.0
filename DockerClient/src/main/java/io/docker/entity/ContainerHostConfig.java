/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.entity;

import com.google.gson.annotations.SerializedName;
import java.util.Map;

/**
 *
 * @author Frantisek Spacek 
 */
public class ContainerHostConfig {

    @SerializedName("Binds")
    private String binds;

    @SerializedName("ContainerIDFile")
    private String containerIdFile;

    @SerializedName("Links")
    private String links;

    @SerializedName("LxcConf")
    private String[] LXCConf;

    @SerializedName("Privileged")
    private boolean privileged;

    @SerializedName("PublishAllPorts")
    private boolean publishAllPorts;

    @SerializedName("PortBindings")
    private Map<String, NetworkPort[]> portBindings;

    public String getBinds() {
        return binds;
    }

    public void setBinds(String binds) {
        this.binds = binds;
    }

    public String getContainerIdFile() {
        return containerIdFile;
    }

    public void setContainerIdFile(String containerIdFile) {
        this.containerIdFile = containerIdFile;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public String[] getLXCConf() {
        return LXCConf;
    }

    public void setLXCConf(String[] LXCConf) {
        this.LXCConf = LXCConf;
    }

    public boolean isPrivileged() {
        return privileged;
    }

    public void setPrivileged(boolean privileged) {
        this.privileged = privileged;
    }

    public boolean isPublishAllPorts() {
        return publishAllPorts;
    }

    public void setPublishAllPorts(boolean publishAllPorts) {
        this.publishAllPorts = publishAllPorts;
    }

    public Map<String, NetworkPort[]> getPortBindings() {
        return portBindings;
    }

    public void setPortBindings(Map<String, NetworkPort[]> portBindings) {
        this.portBindings = portBindings;
    }

    @Override
    public String toString() {
        return "ContainerHostConfig{" + "binds="
                + binds + ", containerIdFile=" + containerIdFile
                + ", links=" + links + ", LXCConf=" + LXCConf
                + ", privileged=" + privileged + ", publishAllPorts="
                + publishAllPorts + ", portBindings=" + portBindings + '}';
    }
}
