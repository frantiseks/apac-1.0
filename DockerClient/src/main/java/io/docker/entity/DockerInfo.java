/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.entity;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Frantisek Spacek
 */
public class DockerInfo {

    @SerializedName("Debug")
    private int debug;

    @SerializedName("Containers")
    private int containers;

    @SerializedName("Images")
    private int images;

    @SerializedName("Driver")
    private String driver;

    @SerializedName("NFd")
    private int NFd;

    @SerializedName("NGRoutines")
    private int NGRoutines;

    @SerializedName("MemoryLimit")
    private int memoryLimit;

    @SerializedName("IPv4Forwarding")
    private int IPv4Forwarding;

    @SerializedName("ExecutionDriver")
    private String executionDriver;

    @SerializedName("NEventsListener")
    private int NEventsListener;

    @SerializedName("KernelVersion")
    private String kernelVersion;

    @SerializedName("IndexServerAddress")
    private String indexServerAddress;

    public int isDebug() {
        return debug;
    }

    public void setDebug(int debug) {
        this.debug = debug;
    }

    public int getContainers() {
        return containers;
    }

    public void setContainers(int containers) {
        this.containers = containers;
    }

    public int isImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public int getNFd() {
        return NFd;
    }

    public void setNFd(int NFd) {
        this.NFd = NFd;
    }

    public int getNGRoutines() {
        return NGRoutines;
    }

    public void setNGRoutines(int NGRoutines) {
        this.NGRoutines = NGRoutines;
    }

    public int isMemoryLimit() {
        return memoryLimit;
    }

    public void setMemoryLimit(int memoryLimit) {
        this.memoryLimit = memoryLimit;
    }

    public int isIPv4Forwarding() {
        return IPv4Forwarding;
    }

    public void setIPv4Forwarding(int IPv4Forwarding) {
        this.IPv4Forwarding = IPv4Forwarding;
    }

    public String getExecutionDriver() {
        return executionDriver;
    }

    public void setExecutionDriver(String executionDriver) {
        this.executionDriver = executionDriver;
    }

    public int getNEventsListener() {
        return NEventsListener;
    }

    public void setNEventsListener(int NEventsListener) {
        this.NEventsListener = NEventsListener;
    }

    public String getKernelVersion() {
        return kernelVersion;
    }

    public void setKernelVersion(String kernelVersion) {
        this.kernelVersion = kernelVersion;
    }

    public String getIndexServerAddress() {
        return indexServerAddress;
    }

    public void setIndexServerAddress(String indexServerAddress) {
        this.indexServerAddress = indexServerAddress;
    }

    @Override
    public String toString() {
        return "DockerInfo{" + "debug=" + debug + ", containers=" + containers + ", images=" + images + ", driver=" + driver + ", NFd=" + NFd + ", NGRoutines=" + NGRoutines + ", memoryLimit=" + memoryLimit + ", IPv4Forwarding=" + IPv4Forwarding + ", LXCVersion=" + executionDriver + ", NEventsListener=" + NEventsListener + ", kernelVersion=" + kernelVersion + ", indexServerAddress=" + indexServerAddress + '}';
    }

}
