/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.entity;

import com.google.gson.annotations.SerializedName;
import java.util.Map;

/**
 *
 * @author Frantisek Spacek
 */
public class NetworkSettings {

    @SerializedName("IPAddress")
    private String ipAddress;
    @SerializedName("IPPrefixLen")
    private int ipPrefixLen;
    @SerializedName("Gateway")
    private String gateway;
    @SerializedName("Bridge")
    private String bridge;
    @SerializedName("PortMapping")
    private Map<String, Map<String, String>> portMapping;

    @SerializedName("Ports")
    private Map<String, NetworkPort[]> ports;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getIpPrefixLen() {
        return ipPrefixLen;
    }

    public void setIpPrefixLen(int ipPrefixLen) {
        this.ipPrefixLen = ipPrefixLen;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getBridge() {
        return bridge;
    }

    public void setBridge(String bridge) {
        this.bridge = bridge;
    }

    public Map<String, Map<String, String>> getPortMapping() {
        return portMapping;
    }

    public void setPortMapping(Map<String, Map<String, String>> portMapping) {
        this.portMapping = portMapping;
    }

    public Map<String, NetworkPort[]> getPorts() {
        return ports;
    }

    public void setPorts(Map<String, NetworkPort[]> ports) {
        this.ports = ports;
    }

    @Override
    public String toString() {
        return "NetworkSettings{" + "ipAddress=" + ipAddress + ", ipPrefixLen=" + ipPrefixLen + ", gateway=" + gateway + ", bridge=" + bridge + ", portMapping=" + portMapping + ", ports=" + ports + '}';
    }
}
