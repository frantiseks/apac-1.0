package io.docker.entity;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class ContainerProcesses {

    @SerializedName("Titles")
    private List<String> titles;

    @SerializedName("Processes")
    private List<String[]> processes;

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List<String[]> getProcesses() {
        return processes;
    }

    public void setProcesses(List<String[]> processes) {
        this.processes = processes;
    }

    @Override
    public String toString() {
        return "ContainerProcesses{" + "titles=" + titles + ", processes=" + processes + '}';
    }

}
