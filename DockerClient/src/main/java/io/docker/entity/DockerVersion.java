/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.entity;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Frantisek Spacek
 */
public class DockerVersion {

    @SerializedName("Version")
    private String version;

    @SerializedName("GitCommit")
    private String gitCommit;

    @SerializedName("GoVersion")
    private String goVersion;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGitCommit() {
        return gitCommit;
    }

    public void setGitCommit(String gitCommit) {
        this.gitCommit = gitCommit;
    }

    public String getGoVersion() {
        return goVersion;
    }

    public void setGoVersion(String goVersion) {
        this.goVersion = goVersion;
    }

    @Override
    public String toString() {
        return "DockerVersion{" + "version=" + version + ", gitCommit=" + gitCommit + ", goVersion=" + goVersion + '}';
    }
}
