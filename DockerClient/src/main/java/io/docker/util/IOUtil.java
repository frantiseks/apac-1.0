/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.docker.util;

import java.io.IOException;
import java.util.Scanner;
import org.apache.http.HttpResponse;

/**
 *
 * @author Frantisek Spacek
 */
public final class IOUtil {

    /**
     * String output from HttpResponse
     *
     * @param response
     * @return
     * @throws IOException
     */
    public static String toString(HttpResponse response) throws IOException {
        Scanner scanner = new Scanner(response.getEntity().getContent()).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }
}
