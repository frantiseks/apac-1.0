package io.docker.client;

import com.google.gson.Gson;
import io.docker.DockerException;
import io.docker.DockerNotRunningException;
import io.docker.entity.Container;
import io.docker.entity.ContainerConfig;
import io.docker.entity.ContainerInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @author Frantisek Spacek
 */
public class UnixDockerClientTest {

    private static DockerClient dockerClient;
    private static final String DEFAULT_CONTAINER_CONFIG = "{\"Image\":\"frantiseks/apac\",\"Memory\":100000000,\"CpuShares\":50,\"Tty\":true}";

    @Before
    public void testInit() throws IOException {
        dockerClient = new DockerClientImpl("unix", "/var/run/docker.sock", 80);
    }

    @Test
    public void testGetContainers() throws DockerException, DockerNotRunningException, URISyntaxException, IOException {
        List<Container> containers = dockerClient.getContainers(true);
        containers.addAll(dockerClient.getContainers(false));
        containers.addAll(dockerClient.getContainers(true));
        Assert.assertNotNull(containers);
        //Assert.assertTrue(containers.size() > 0);
        containers.stream().forEach((c) -> {
            System.out.println(c.toString());
        });
    }

    @Test
    @Ignore
    public void testDeleteContainer() throws DockerException, DockerNotRunningException {

        List<Container> containers = dockerClient.getContainers(false);
        Assert.assertNotNull(containers);
        //Assert.assertTrue(containers.size() > 0);

        Container c = containers.get(0);
        //dockerClient.deleteContainer(c.getId());
    }

    @Test
    public void testInspectContainer() throws DockerException, DockerNotRunningException {
        List<Container> containers = dockerClient.getContainers(false);
        Assert.assertNotNull(containers);
        Assert.assertTrue(containers.size() > 0);

        for (Container c : containers) {
            System.out.println("Container id" + c.getId());
            System.out.println(dockerClient.inspectContainer(c.getId()));
        }
    }

    @Test
    public void testCreateContainer() throws Exception {
        ContainerInfo container = dockerClient.createContainer(new Gson().fromJson(DEFAULT_CONTAINER_CONFIG, ContainerConfig.class));

        System.out.println("Created container " + container.toString());

        //dockerClient.deleteContainer(container.getId());
    }

    @Test
    public void testGetContainerProcesses() throws Exception {
        List<Container> containers = dockerClient.getContainers(false);
        Assert.assertNotNull(containers);
        Assert.assertTrue(containers.size() > 0);

        for (Container c : containers) {
            System.out.println("Container id" + c.getId());
            System.out.println(new Gson().toJson(dockerClient.getContainerProcesses(c.getId(), "aux")));
        }

    }

    @Test
    public void testStartContainer() throws Exception {
        dockerClient.startContainer("f0072d405cd27eb891ba5d116f2810c4191149b9274a9113491fb09be1506505");
    }
//    
//    @Test
//    public void testSerializationNetworkPort() throws JsonProcessingException {
//        Map<String, NetworkPort[]> map = new HashMap<>();
//        NetworkPort networkPort = new NetworkPort();
//        networkPort.setHostIp("0.0.0.0");
//        networkPort.setHostPort("49155");
//
//        map.put("22/tcp", new NetworkPort[]{networkPort});
//
//        ObjectMapper om = new ObjectMapper();
//        System.out.println(om.writeValueAsString(map));
//    }

    @Test
    public void testGetInfo() throws DockerException, DockerNotRunningException, URISyntaxException, IOException {
        System.out.println(new Gson().toJson(dockerClient.getInfo()));
    }
}
