clientApp.factory('Assignments', function($resource) {
    return $resource(assignmentUrl, {}, {
        get: {
            method: 'GET',
            params: {
                id: '@id'
            },
            url: assignmentUrl + "/:id"
        },
        create: {
            method: 'POST',
            url: assignmentUrl
        },
        update: {
            method: 'PUT',
            params: {
                id: '@id'
            },
            url: assignmentUrl + "/:id"
        },
        referenceSubmissions: {
            method: 'GET',
            isArray: true,
            params: {
                id: '@id'
            },
            url: assignmentUrl + "/:id/references"
        },
        upload: {
            method: 'POST',
            params: {
                id: '@id',
                filename: '@filename'
            },
            headers: {
                'Content-Type': 'application/octet-stream'
            },
            url: assignmentUrl + "/:id/upload"
        }
    });
});

clientApp.factory('Submissions', function($resource) {
    return $resource(submissionUrl + "/:uuid", {
        uuid: '@uuid'
    });
});

clientApp.factory('Submissions', function($resource) {
    return $resource(submissionUrl, {}, {
        upload: {
            method: 'POST',
            headers: {
                'Content-Type': 'application/octet-stream'
            }
        }
    });
});


clientApp.factory('Executions', function($resource) {
    return $resource(executionUrl);
});

clientApp.factory('Vectors', function($resource) {
    return $resource(vectorUrl);
});

clientApp.factory('Vectors', function($resource) {
    return $resource(vectorUrl, {}, {
        get: {
            method: 'GET',
            params: {
                id: '@id'
            },
            url: vectorUrl + "/:id"
        },
        del: {
            method: 'DELETE',
            params: {
                id: '@id'
            },
            url: vectorUrl + "/:id"
        },
        update: {
            method: 'PUT',
            params: {
                id: '@id'
            },
            url: vectorUrl + "/:id"
        },
        upload: {
            method: 'POST',
            params: {
                id: '@id'
            },
            headers: {
                'Content-Type': 'application/octet-stream'
            },
            url: vectorUrl + "/:id/upload"
        }
    });
});

clientApp.factory('Administrations', function($resource) {
    return $resource(adminUrl);
});

clientApp.factory('Containers', function($resource) {
    return $resource(containersUrl);
});

clientApp.factory('Containers', function($resource) {
    return $resource(containersUrl, {}, {
        list: {
            method: 'GET',
            isArray: true,
            url: containersUrl
        },
        restart: {
            method: 'POST',
            params: {
                containerId: '@containerId'
            },
            url: containersUrl + '/:containerId/restart'
        },
        kill: {
            method: 'POST',
            params: {
                containerId: '@containerId'
            },
            url: containersUrl + '/:containerId/kill'
        },
        stop: {
            method: 'POST',
            params: {
                containerId: '@containerId'
            },
            url: containersUrl + '/:containerId/stop'
        },
        del: {
            method: 'DELETE',
            params: {
                containerId: '@containerId'
            },
            url: containersUrl + '/:containerId/delete'
        },
        detail: {
            method: 'GET',
            params: {
                containerId: '@containerId'
            },
            url: containersUrl + '/:containerId/'
        },
        processes: {
            method: 'GET',
            params: {
                containerId: '@containerId'
            },
            url: containersUrl + '/:containerId/processes'
        }
    });
});


clientApp.factory('Tasks', function($resource) {
    return $resource(tasksUrl, {}, {
        list: {
            method: 'GET',
            isArray: true,
            url: tasksUrl
        },
        restart: {
            method: 'POST',
            params: {
                taskId: '@taskId'
            },
            url: tasksUrl + '/:taskId/restart'
        },
        cancel: {
            method: 'POST',
            params: {
                taskId: '@taskId'
            },
            url: tasksUrl + '/:taskId/cancel'
        }

    });
});

clientApp.factory('Plugins', function($resource) {
    return $resource(pluginsUrl);
});