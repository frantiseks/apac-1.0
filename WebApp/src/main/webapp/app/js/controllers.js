clientApp.controller('dashboardController', ['$scope', 'Submissions', '$location',
    function($scope, Submissions, $location) {
        $scope.data = {};

        Submissions.query({
            days: 7
        }, function(response) {
            $scope.data.submissions = response;
        });

        $scope.go = function(id) {
            $location.url('/submission/' + id);
        };
    }
]);

clientApp.controller('submissionsController', ['$scope', '$location', 'Submissions', 'Assignments',
    function($scope, $location, Submissions, Assignments) {
        $scope.data = {};
        $scope.dateFrom = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        $scope.dateTo = new Date();
        $scope.submissionFile = undefined;

        $scope.selectedAssignment = {};

        Assignments.query({
            years: 1
        }, function(response) {
            console.log("Loaded");
            $scope.assignmentsList = response;

            if ($scope.assignmentsList.length > 0) {
                $scope.selectedAssignment = $scope.assignmentsList[0];
            }
        });


        /* $scope.getAssignments = function () {
         Assignments.query({
         years: 1
         }, function (response) {
         $scope.assignmentsList = response;
         
         if ($scope.assignmentsList.length > 0) {
         $scope.selectedAssignment = $scope.assignmentsList[0];
         }
         });
         };*/

        $scope.getSubmissionsByAssignment = function() {
            Submissions.query({
                assignmentId: $scope.selectedAssignment.id
            }, function(response) {
                $scope.data.submissions = response;
            });
        };
        $scope.onFileSelect = function($files) {
            $scope.submissionFile = $files[0];
        };

        $scope.uploadSubmission = function() {
            Submissions.upload({
                assignmentId: $scope.selectedAssignment.id,
                filename: $scope.submissionFile.name
            }, $scope.submissionFile, function(response) {
                $scope.data.response = response;
                $location.url('/submission/' + response.uuid);
            });
        };

        /* $scope.go = function (path) {
         $location.path(path);
         };*/

        $scope.go = function(id) {
            $location.url('/submission/' + id);
        };

        $scope.open = function($event, i) {
            $event.preventDefault();
            $event.stopPropagation();

            if (i === 1) {
                $scope.opened1 = true;
                $scope.opened2 = false;
            }

            if (i === 2) {
                $scope.opened1 = false;
                $scope.opened2 = true;
            }


        };

        $scope.dateOptions = {
            'year-format': "'yy'",
            'starting-day': 1
        };

        $scope.formats = ['dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.filter = function() {
            Submissions.query({
                assignmentId: $scope.selectedAssignment.id,
                from: $scope.dateFrom.getTime(),
                to: $scope.dateTo.getTime()
            }, function(response) {
                $scope.data.submissions = response;
            });
        };

    }
]);

clientApp.controller('assignmentsController', ['$scope', '$location', '$routeParams', 'Assignments',
    function($scope, $location, $routeParams, Assignments) {
        if ($routeParams.id !== undefined) {
            $scope.tab = 2;
        } else {
            $scope.tab = 1;
        }

        $scope.data = {};
        $scope.getAssignments = function() {
            Assignments.query({
                years: 1
            }, function(response) {
                $scope.data.assignments = response;
            });
        };

        $scope.go = function(id) {
            $location.url('/assignments/' + id);
        };
    }
]);

clientApp.controller('assignmentConfigureController', ['$scope', '$routeParams', '$location', 'Assignments', 'Vectors',
    function($scope, $routeParams, $location, Assignments, Vectors) {
        $scope.alerts = [];

        $scope.addAlert = function(type, message) {
            $scope.alerts.push({type: type, msg: message});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.data = {};
        $scope.edited = false;
        $scope.numLimit = 200;
        $scope.readMore = function() {
            $scope.numLimit = 10000;
        };
        $scope.assignmentForm = {};

        if ($routeParams.id !== undefined) {
            Assignments.get({
                id: $routeParams.id
            }, function(response) {
                $scope.assignmentForm = response;
                $scope.fileSelectLabel = "Update reference solution";
                $scope.assignmentStatus = response.status;
            }, function(response) {
                $scope.assignmentStatus = response.status;
            });
        } else {
            $scope.fileSelectLabel = "Select reference solution";
        }

        $scope.supportedLangs = supportLanguages;
        $scope.assignmentForm.language = supportLanguages[0];

        $scope.onFileSelect = function($files) {
            $scope.baseFile = $files[0];
            console.log($scope.baseFile.name);
        };

        $scope.onSTDINFileSelect = function($files) {
            $scope.argumentForm.STDINFile = $files[0];
            $scope.argumentForm.value = $scope.argumentForm.STDINFile.name;
        };

        $scope.uploadReference = function() {
            // $scope.addAlert("success", "Uploading reference....");
            console.log($scope.baseFile.name);
            Assignments.upload({
                id: $routeParams.id,
                filename: $scope.baseFile.name
            }, $scope.baseFile, function(response) {
                $scope.data.response = response;
                $scope.alerts = [];
                $scope.addAlert("success", "Reference uploaded");
            }, function() {
                $scope.alerts = [];
                $scope.addAlert("danger", "Reference upload failed");
            });
        };

        $scope.saveAssignment = function() {
            if (angular.isUndefined($routeParams.id) || $scope.assignmentStatus === 404) {
                Assignments.create({}, $scope.assignmentForm, function(response) {
                    $scope.goConfig(response.id);
                });
            } else {
                Assignments.update({
                    id: $routeParams.id
                }, $scope.assignmentForm, function(response) {
                    $scope.assignmentForm = response;
                });
            }
        };

        $scope.supportedArgs = supportedArgType;
        $scope.vectorForm = {};
        $scope.argumentForm = {};
        $scope.argumentForm.type = supportedArgType[0];

        $scope.addArg = function() {
            if (angular.isUndefined($scope.vectorForm.arguments)) {
                $scope.vectorForm.arguments = [];
            }
            $scope.vectorForm.arguments.push($scope.argumentForm);
            $scope.argumentForm = {};
            $scope.argumentForm.type = supportedArgType[0];
        };

        $scope.loadVectors = function() {
            if (!angular.isUndefined($routeParams.id)) {
                Vectors.query({
                    assignmentId: $routeParams.id
                }, function(response) {
                    $scope.data.vectors = response;
                });
            }
        };

        $scope.deleteArg = function(index) {
            $scope.vectorForm.arguments.splice(index, 1);
        };

        $scope.editArg = function(index) {
            $scope.argumentForm = $scope.vectorForm.arguments[index];
        };

        $scope.saveVector = function() {
            console.log(angular.toJson($scope.vectorForm));
            if ($scope.edited) {
                Vectors.update({
                    id: $scope.vectorForm.id
                }, $scope.vectorForm, function(response) {
                    $scope.vectorForm.arguments.forEach(function(argument) {
                        if (!angular.isUndefined(argument.STDINFile)) {
                            Vectors.upload({
                                id: response.id,
                                filename: argument.STDINFile.name
                            }, argument.STDINFile, function(response) {
                                $scope.data.response = response;
                                console.log(response);
                            });
                        }
                    });
                    $scope.vectorForm.arguments = [];
                    $scope.loadVectors();
                    $scope.edited = false;
                });
            } else {
                Vectors.save({
                    assignmentId: $routeParams.id
                }, $scope.vectorForm, function(response) {
                    console.log(response);
                    $scope.vectorForm.arguments.forEach(function(argument) {
                        if (!angular.isUndefined(argument.STDINFile)) {
                            Vectors.upload({
                                id: response.id,
                                filename: argument.STDINFile.name
                            }, argument.STDINFile, function(response) {
                                $scope.data.response = response;
                                console.log(response);
                            });
                        }
                    });
                    $scope.vectorForm.arguments = [];
                    $scope.loadVectors();
                    $scope.edited = false;
                });
            }
        };

        $scope.editVector = function(id) {
            Vectors.get({
                id: id
            }, function(response) {
                console.log(angular.toJson(response));
                $scope.vectorForm = response;
            });
            $scope.edited = true;
        };

        $scope.deleteVector = function(id) {
            Vectors.del({
                id: id
            }, angular.noop, function(response) {
                console.log(angular.toJson(response.status));
                $scope.status = response.status;
                $scope.loadVectors();
            });
        };

        $scope.loadReferenceSubmissions = function() {
            if (angular.isUndefined($routeParams.id)) {
                $scope.data.referenceSubmissions = [];
            } else {
                Assignments.referenceSubmissions({
                    id: $routeParams.id
                }, function(response) {
                    $scope.data.referenceSubmissions = response;
                    console.log(response);
                });
            }
        };

        $scope.go = function(id) {
            $location.url('/submission/' + id);
        };

        $scope.goConfig = function(id) {
            $location.url('/assignments/' + id);
        };

    }
]);

clientApp.controller('submissionDetailController', ['$scope', '$resource', 'Executions', '$routeParams',
    function($scope, $resource, Executions, $routeParams) {
        $scope.data = {};
        $scope.numLimit = 200;
        $scope.readMore = function() {
            $scope.numLimit = 10000;
        };

        $scope.loadDetails = function() {

            var submission = $resource(submissionUrl + "/:uuid");
            submission.get({
                uuid: $routeParams.id
            }, function(response) {
                $scope.data.submission = response;
            });
        };

        $scope.loadExecutions = function() {
            Executions.query({
                uuid: $routeParams.id
            }, function(response) {
                $scope.data.executions = response;
            });
        };
    }
]);

clientApp.controller('adminController', ['$scope', 'Containers', 'Tasks', 'Plugins',
    function($scope, Containers, Tasks, Plugins) {
        $scope.data = {};
        $scope.containerDetail = false;

        $scope.loadContainers = function() {
            Containers.list(function(response) {
                $scope.data.containers = response;
            });
        };

        $scope.loadTasks = function() {
            Tasks.list(function(response) {
                $scope.data.tasks = response;
            });
        };

        $scope.loadPlugins = function() {
            Plugins.query(function(response) {
                $scope.data.plugins = response;
            });
        };

        $scope.restartContainer = function(id) {
            Containers.restart({
                containerId: id
            }, null, function(response) {
                $scope.data.response = response;
                $scope.loadContainers();
            });
        };

        $scope.stopContainer = function(id) {
            Containers.stop({
                containerId: id
            }, null, function(response) {
                $scope.data.response = response;
                $scope.loadContainers();
            });
        };

        $scope.killContainer = function(id) {
            Containers.kill({
                containerId: id
            }, null, function(response) {
                $scope.data.response = response;
                $scope.loadContainers();
            });
        };

        $scope.deleteContainer = function(id) {
            Containers.del({
                containerId: id
            }, null, function(response) {
                $scope.data.response = response;
                $scope.loadContainers();
            });
        };

        $scope.restartJob = function(id) {
            Tasks.restart({
                taskId: id
            }, null, function(response) {
                $scope.data.response = response;
            });
        };

        $scope.cancelJob = function(id) {
            Tasks.cancel({
                taskId: id
            }, null, function(response) {
                $scope.data.response = response;
            });
        };

        $scope.detail = function(id) {
            Containers.detail({containerId: id}, null, function(response) {
                $scope.data.detail = response;
                $scope.containerDetail = true;
            });
        };

        $scope.loadProcesses = function(id) {
            Containers.processes({containerId: id}, null, function(response) {
                $scope.data.detail.processes = response;
            });
        };
    }
]);
