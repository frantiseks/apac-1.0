'use strict';
/** configuration **/
var baseUrl = "http://localhost:8080/apac/api/";

var submissionUrl = baseUrl + "submission";
var assignmentUrl = baseUrl + "assignment";
var executionUrl = baseUrl + "execution";
var vectorUrl = baseUrl + "vector";
var adminUrl = baseUrl + "admin";
var containersUrl = adminUrl + "/container";
var tasksUrl = baseUrl + "tasks";
var pluginsUrl = adminUrl + "/plugin";
var supportLanguages = ["C", "CPP", "JAVA"];
var supportedArgType = ["INPUTFILE", "STRING", "OUTPUTFILE", "STDINFILE"];

var clientApp = angular.module('clientApp', ['ngRoute', 'ngResource', 'angularFileUpload', 'ui.bootstrap']);
clientApp.config(function($routeProvider) {
    $routeProvider
            // route for the home page
            .when('/', {
                templateUrl: 'partials/dashboard.html',
                controller: 'dashboardController'
            })
            .when('/assignments', {
                templateUrl: 'partials/assignments.html',
                controller: 'assignmentsController'
            })
            .when('/assignments/:id', {
                templateUrl: 'partials/assignments.html',
                controller: 'assignmentConfigureController'
            })

            .when('/submission/:id', {
                templateUrl: 'partials/submission_detail.html',
                controller: 'submissionDetailController'
            })
            .when('/admin', {
                templateUrl: 'partials/administration.html',
                controller: 'adminController'
            })
            // route for the contact page
            .when('/submissions', {
                templateUrl: 'partials/submissions.html',
                controller: 'submissionsController'
            });
});
