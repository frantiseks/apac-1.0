/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.batch;

import com.jcraft.jsch.SftpException;
import cz.utb.fai.apac.builder.ArgumentsBuilder;
import cz.utb.fai.apac.builder.CommandBuilder;
import cz.utb.fai.apac.database.VectorService;
import cz.utb.fai.apac.entity.Assignment;
import cz.utb.fai.apac.entity.Execution;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.enums.Extension;
import cz.utb.fai.apac.enums.ResponseCode;
import cz.utb.fai.apac.processor.SandboxSession;
import cz.utb.fai.apac.processor.SandboxSessionPool;
import cz.utb.fai.apac.processor.SubmissionProcessorFactory;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import cz.utb.fai.apac.util.FormatUtil;
import cz.utb.fai.apac.util.GenericExtFilter;
import cz.utb.fai.apac.util.SandboxUtil;
import cz.utb.fai.apac.util.VectorUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.api.AbstractBatchlet;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author František Špaček
 */
@Named
public class ExecutionStep extends AbstractBatchlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionStep.class);
    private int maxExecutionRuntime;
    private SubmissionProcessor processor;

    @Inject
    private JobContext jobContext;

    @Inject
    private SubmissionProcessorFactory processorFactory;

    @Override
    public String process() {
        String exitStatus = String.valueOf(ExitStatus.EXECUTION_FAILED);

        //fetch meta data of job
        Submission jobData = (Submission) jobContext.getTransientUserData();
        String language = jobData.getAssignment().getLanguage();
        LOGGER.debug("Submission language is {}", language);

        maxExecutionRuntime = jobData.getAssignment().getMaxRuntime();

        //get instance of supported language
        processor = processorFactory.getInstance(language);

        if (processor != null) {

            //get workspace dir from jobContext
            File submissionWorkspace = jobData.getSubmissionWorkspace();

            SandboxSession sandbox = null;
            try {

                //fetch test vectors
                List<Vector> testVectors = VectorService.getAllByAssignmentId(jobData.getAssignment().getId());

                //try to aquire sandbox instance
                sandbox = SandboxSessionPool.getSandbox();

                List<Execution> executionResults = new ArrayList<>();
                if (testVectors == null || testVectors.isEmpty()) {
                    //Vectors was not found, execute assignment
                    executionResults.add(executeSubmission(sandbox, submissionWorkspace, jobData.getAssignment(), null));
                } else {

                    for (Vector vector : testVectors) {

                        //if is not SSH session still alive, end of testing 
                        if (!SandboxUtil.isSessionAlive(sandbox)) {
                            break;
                        }
                        //execute testing with vector
                        executionResults.add(executeSubmission(sandbox, submissionWorkspace, jobData.getAssignment(), vector));

                    }

                    LOGGER.debug("Execution results {}", FormatUtil.json(executionResults));

                    jobData.setExecutions(executionResults);
                    exitStatus = String.valueOf(ExitStatus.EXECUTION_SUCCESS);
                }
            } catch (Exception ex) {
                LOGGER.error("Execution Step ex", ex);
            } finally {
                if (sandbox != null) {
                    //return sandbox instance to pool
                    SandboxSessionPool.returnSandbox(sandbox);
                }
            }

        } else {
            exitStatus = String.valueOf(ExitStatus.LANGUAGE_NOT_SUPPORTED);
        }

        jobContext.setExitStatus(exitStatus);
        return exitStatus;
    }

    private Execution executeSubmission(SandboxSession sandbox, File submissionWorkspace, Assignment assignment, Vector vector) throws SftpException, Exception {
        Execution result = new Execution(ResponseCode.Executor.ERROR);

        List<String> commands = new ArrayList<>();
        commands.add(String.format("chmod +x %s.build", submissionWorkspace.getName()));

        List<File> files = new ArrayList<>();
        //if vector was found
        if (vector != null) {
            String stdinFileName = VectorUtil.getSTDINFileName(vector);
            if (!stdinFileName.isEmpty()) {
                File stdinFile = new File(assignment.getBaseFile().getParent() + File.separator + stdinFileName);
                FileUtils.copyFileToDirectory(stdinFile, submissionWorkspace);
            }

            //get binary file, and input files for testing
            files = Arrays.asList(submissionWorkspace
                    .listFiles(new GenericExtFilter(
                                    new String[]{Extension.BUILD.toString(), Extension.IN.toString(), stdinFileName})));

            //build arguments 
            String args = ArgumentsBuilder.build(vector.getArguments(), submissionWorkspace.getAbsolutePath());
            commands.add(String.format("%s.%s %s", String.format(processor.executionCommand(), submissionWorkspace.getName()), Extension.BUILD.toString(), args.trim()));

            //add commands for read output files
            if (args.contains("." + Extension.OUT.toString())) {
                commands.add("cat *." + Extension.OUT.toString());
                commands.add("rm -rf *." + Extension.OUT.toString());
            }
        } else {
            commands.add(String.format("%s %s.%s", processor.executionCommand(), submissionWorkspace, Extension.BUILD.toString()));
        }

        commands.add(String.format("rm -rf *.%s *.%s", Extension.BUILD.toString(), Extension.IN.toString()));

        //build command
        String executionCommand = CommandBuilder.build(commands);
        LOGGER.info("Execution command is {}", executionCommand);

        if (executionCommand != null) {

            LOGGER.debug("Putting files {} to sandbox", FormatUtil.json(files));
            SandboxUtil.putFiles(sandbox, files);

            LOGGER.info("Executing command {} in sandbox {}", executionCommand, sandbox.getContainerId());
            result.setOutput(SandboxUtil.executeCommand(sandbox, executionCommand, maxExecutionRuntime));

            result.setOutputMatchTreshold(assignment.getThreshold());
            result.setTerminated(result.getOutput().contains("killed"));

            if (vector != null) {
                result.setVectorId(vector.getId());
                result.setVector(vector);
            }
        }

        return result;
    }
}
