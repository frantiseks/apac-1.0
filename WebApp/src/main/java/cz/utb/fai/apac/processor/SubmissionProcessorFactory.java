/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.spi.ProcessorInfo;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.GenericExtFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.File;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 *
 * @author František Špaček
 */
@Singleton
@Startup
@DependsOn("ConfigurationUtil")
public class SubmissionProcessorFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubmissionProcessorFactory.class);
    private static ServiceLoader<SubmissionProcessor> processorLoader;
    private static FileAlterationMonitor monitor;

    @PostConstruct
    public void init() {
        try {
            File processorsDir = new File(ConfigurationUtil.getProperty("apac.pluginsDir"));
            loadPlugins(processorsDir);

            //plugin dir changes listener
            FileAlterationObserver directoryObserver = new FileAlterationObserver(processorsDir);
            directoryObserver.addListener(new FileAlterationListenerImpl());

            monitor = new FileAlterationMonitor();
            monitor.addObserver(directoryObserver);

            LOGGER.info("Starting monitoring plugin directory");
            monitor.start();
        } catch (Exception ex) {
            LOGGER.error("SubmissionProcessorFactory", ex);
        }
    }

    @PreDestroy
    public void destroy() {
        try {
            LOGGER.info("Stopping plugin directory monitor.");
            monitor.stop();
        } catch (Exception ex) {
            LOGGER.error("Monitor shutdown", ex);
        }
    }

    public void reload() {
        processorLoader.reload();
    }

    public SubmissionProcessor getInstance(String language) {
        for (SubmissionProcessor subProcessor : processorLoader) {
            if (subProcessor.getClass().isAnnotationPresent(ProcessorInfo.class)) {
                Annotation annotation = subProcessor.getClass().getAnnotation(ProcessorInfo.class);
                ProcessorInfo processorInfo = (ProcessorInfo) annotation;

                if (processorInfo.language().equals(language)) {
                    return subProcessor;
                }
            }
        }

        return null;
    }

    public static List<String> getLoadedPlugins() {
        List<String> plugins = new ArrayList();

        for (SubmissionProcessor submissionProcessor : processorLoader) {
            plugins.add(submissionProcessor.getClass().getSimpleName());
        }

        return plugins;
    }

    private void loadPlugins(File processorsDir) {
        LOGGER.info("Loading apac plugins from dir {} ", processorsDir.getAbsolutePath());
        File[] jars = processorsDir.listFiles(new GenericExtFilter(new String[]{"jar"}));
        if (jars == null) {
            LOGGER.info("No apac plugins found");
            return;
        }
        List<URL> jarUrls = new ArrayList<>();
        try {
            for (File jar : jars) {
                LOGGER.info("Loading plugin jar {}", jar.getName());
                jarUrls.add(jar.toURI().toURL());
            }

        } catch (MalformedURLException ex) {
            LOGGER.error("SubmissionProcessorFactor init ex", ex);
        }

        URLClassLoader jarClassLoader = new URLClassLoader(jarUrls.toArray(new URL[jarUrls.size()]), Thread.currentThread().getContextClassLoader());
        processorLoader = ServiceLoader.load(SubmissionProcessor.class, jarClassLoader);
    }

    private class FileAlterationListenerImpl implements FileAlterationListener {

        @Override
        public void onStart(FileAlterationObserver fao) {
            LOGGER.debug("Starting directory monitoring");
        }

        @Override
        public void onDirectoryCreate(File file) {
            LOGGER.debug("Plugin directory change");
        }

        @Override
        public void onDirectoryChange(File file) {
            LOGGER.debug("Plugin directory change");
        }

        @Override
        public void onDirectoryDelete(File file) {
            LOGGER.debug("Plugin directory change");
        }

        @Override
        public void onFileCreate(File file) {
            LOGGER.debug("New plugin found, reloading");
            loadPlugins(file.getParentFile());
        }

        @Override
        public void onFileChange(File file) {
            LOGGER.debug("Plugin change was detected");
            loadPlugins(file.getParentFile());
        }

        @Override
        public void onFileDelete(File file) {
            LOGGER.debug("Plugin has been deleted, reloading");
            loadPlugins(file.getParentFile());
        }

        @Override
        public void onStop(FileAlterationObserver fao) {
            LOGGER.debug("Stopping directory monitoring");
        }
    }
}
