/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.dto;

import cz.utb.fai.apac.entity.Argument;
import cz.utb.fai.apac.entity.Vector;

import java.util.List;

/**
 *
 * @author František Špaček
 */
public class VectorDTO {

    private final Long id;
    private final String output;
    private final double points;
    private final List<Argument> arguments;

    public VectorDTO(Vector vector) {
        this.id = vector.getId();
        this.output = vector.getOutput();
        this.points = vector.getPoints();
        this.arguments = vector.getArguments();
    }

    public long getId() {
        return id;
    }

    public String getOutput() {
        return output;
    }

    public double getPoints() {
        return points;
    }

    public List<Argument> getArguments() {
        return arguments;
    }
    
    public Vector toVector(){
        Vector v = new Vector();
        v.setArguments(arguments);
        v.setOutput(output);
        v.setPoints(points);
        
        return v;
    }

    @Override
    public String toString() {
        return "VectorDTO{" + "id=" + id + ", output=" + output 
                + ", points=" + points + ", arguments=" + arguments + '}';
    }

}
