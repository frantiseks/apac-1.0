/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.batch.ExecuteEvalution;
import cz.utb.fai.apac.database.AssignmentService;
import cz.utb.fai.apac.database.SubmissionService;
import cz.utb.fai.apac.database.VectorService;
import cz.utb.fai.apac.dto.AssignmentDTO;
import cz.utb.fai.apac.dto.SubmissionDTO;
import cz.utb.fai.apac.entity.Assignment;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.FormatUtil;
import cz.utb.fai.apac.util.ZipUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipException;

/**
 *
 * @author František Špaček
 */
@Path("assignment")
@Produces(MediaType.APPLICATION_JSON)
public class AssignmentResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssignmentResource.class);

    @GET
    public Response getAssignments(@QueryParam("years") Integer years) {
        Response result = Response.serverError().build();
        try {
            //if filter was not specified, parameter years are set to one year
            int filter = years == null ? 1 : years;

            List<Assignment> assignments = AssignmentService.getAll(filter);
            List<AssignmentDTO> dtos = new ArrayList<>();

            for (Assignment a : assignments) {
                dtos.add(new AssignmentDTO(a));
            }

            result = Response.ok(dtos).build();

        } catch (SQLException ex) {
            LOGGER.error("Failed to fetch assignments", ex);
        }

        return result;
    }

    @GET
    @Path("{assignmentId}")
    public Response getAssignment(@PathParam("assignmentId") long id) {
        Response result = Response.serverError().build();
        try {
            Assignment assignment = AssignmentService.getById(id);
            if (assignment == null) {
                result = Response.status(Response.Status.NOT_FOUND).build();
            } else {
                result = Response.ok().entity(new AssignmentDTO(assignment)).build();
            }

        } catch (SQLException ex) {
            LOGGER.error("Failed to fetch assignment with id {}", id, ex);
        }
        return result;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createAssignment(@NotNull @Valid AssignmentDTO assignment) {
        Response result = Response.serverError().build();

        try {
            Assignment created = AssignmentService.insert(assignment.getAssignment());
            if (created != null) {
                result = Response.ok(new AssignmentDTO(created)).build();
            }
        } catch (SQLException ex) {
            LOGGER.error("Failed to insert assignment {}", FormatUtil.json(assignment), ex);
        }

        return result;
    }

    @PUT
    @Path("{assignmentId}")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateAssignment(@NotNull @Valid AssignmentDTO assignment, @PathParam("assignmentId") long assignmentId) {
        Response result = Response.serverError().build();

        try {
            assignment.setId(assignmentId);

            if (assignment.getId() != 0) {

                Assignment updated = AssignmentService.update(assignment.getAssignment());
                if (updated != null) {
                    result = Response.ok(new AssignmentDTO(updated)).build();
                }
            } else {
                result = Response.notModified().build();
            }
        } catch (SQLException ex) {
            LOGGER.error("Failed to update assignment {}", FormatUtil.json(assignment), ex);
        }

        return result;
    }

    @GET
    @Path("{assignmentId}/references")
    public Response getReferenceSubmissions(@PathParam("assignmentId") long id) {
        Response result = Response.serverError().build();

        try {
            Assignment assignment = AssignmentService.getById(id);
            if (assignment == null) {
                result = Response.status(Response.Status.NOT_FOUND).build();
            } else {
                List<Submission> referenceSubmissions = SubmissionService.getReferenceSubmissions(id);

                List<SubmissionDTO> dtos = new ArrayList<>();
                for (Submission s : referenceSubmissions) {
                    dtos.add(new SubmissionDTO(s, true));
                }

                result = Response.ok(dtos).build();
            }

        } catch (SQLException ex) {
            LOGGER.error("Failed to fetch assignment with id {}", id, ex);
        }

        return result;
    }

    @POST
    @Path("{assignmentId}/upload")
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    public Response uploadReference(@PathParam("assignmentId") long id, 
            @NotNull File file, 
            @NotNull @Size(min = 1) @QueryParam("filename") String filename) {
        Response result = Response.serverError().build();

        if (file.length() == 0) {
            result = Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            try {
                if (!ZipUtil.isZip(file)) {
                    result = Response.status(Response.Status.BAD_REQUEST).build();
                } else {
                    Assignment assignment = AssignmentService.getById(id);

                    if (assignment != null) {
                        String refDir = ConfigurationUtil.getProperty("apac.refDir");
                        File referenceFolder = new File(String.format("%s%s", refDir, id));

                        if (!referenceFolder.exists()) {
                            referenceFolder.mkdirs();
                        }

                        File referenceFile = new File(String
                                .format("%s%s%s", referenceFolder.getAbsolutePath(), File.separator, filename.isEmpty() ? new Date().getTime() : filename));

                        //copy file to assignments references dir
                        FileUtils.copyFile(file, referenceFile);

                        AssignmentService.updateBaseFile(id, referenceFile.getAbsolutePath());

                        if (VectorService.countByAssignmentId(id) > 0) {
                            //if some vectors are already defined, execute reference submission
                            ExecuteEvalution.execute(referenceFile, new Submission().getWorkspaceUUID(), id, true, referenceFile.getName());
                        }
                        result = Response.ok().build();
                    }
                }
            } catch (SQLException ex) {
                LOGGER.error("Failed to fetch assignment with id {}", id, ex);
            } catch (ZipException ex) {
                LOGGER.error("Uploaded file has incorrect zip format", ex);
                result = Response.status(Status.NOT_ACCEPTABLE).build();
            } catch (IOException ex) {
                LOGGER.error("Failed to create file", ex);
            }
        }
        return result;
    }

    @DELETE
    @Path("{assignmentId}")
    public Response deleteAssignment(@PathParam("assignmentId") long assignmentId) {
        Response result = Response.serverError().build();
        try {
            Assignment assignment = AssignmentService.getById(assignmentId);
            if (assignment != null) {
                //test if there is no submission or defined vector 
                if (SubmissionService.countByAssignmentId(assignmentId) == 0
                        && VectorService.countByAssignmentId(assignmentId) == 0) {
                    AssignmentService.delete(assignmentId);
                    result = Response.ok().build();
                } else {
                    //assignment can not be deleted
                    result = Response.status(Status.CONFLICT).build();
                }
            } else {
                result = Response.status(Status.NOT_FOUND).build();
            }

        } catch (SQLException ex) {

        }

        return result;
    }
}
