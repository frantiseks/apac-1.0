/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.database;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import cz.utb.fai.apac.entity.Argument;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.util.DatabaseUtil;
import cz.utb.fai.apac.util.FormatUtil;

import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class VectorService {

    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM vectors WHERE id = ?";
    private static final String SELECT_ALL_BY_ASSIGNMENT_ID_QUERY = "SELECT * FROM vectors WHERE assignment_id = ?";
    private static final String INSERT_QUERY = "INSERT INTO vectors (arguments, points, assignment_id) VALUES(?, ?, ?)";
    private static final String UPDATE_OUTPUT_QUERY = "UPDATE vectors SET output = ? WHERE id = ?";
    private static final String UPDATE_QUERY = "UPDATE vectors SET points = ?, arguments = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM vectors WHERE id = ?";
    private static final String COUNT_BY_ASSIGNMENT_ID_QUERY = "SELECT count(*) FROM vectors WHERE assignment_id = ?";

    public static Vector getById(long id) throws SQLException, JsonSyntaxException {
        Vector vector = null;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_BY_ID_QUERY)) {

            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                vector = new Vector();
                vector.setId(result.getLong("id"));
                vector.setOutput(result.getString("output"));
                vector.setPoints(result.getDouble("points"));
                vector.setAssignmentId(result.getLong("assignment_id"));

                //arguments are stored as json stringobe
                String argumentsJson = result.getString("arguments");

                if (argumentsJson != null && !argumentsJson.isEmpty()) {
                    Type type = new TypeToken<Collection<Argument>>() {
                    }.getType();

                    List<Argument> arguments = FormatUtil.fromJson(argumentsJson, type);
                    vector.setArguments(arguments);
                }
            }
        }
        return vector;
    }

    public static List<Vector> getAllByAssignmentId(long assignmentId) throws SQLException, JsonSyntaxException {
        List<Vector> vectors = new ArrayList<>();

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_BY_ASSIGNMENT_ID_QUERY)) {

            statement.setLong(1, assignmentId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Vector vector = new Vector();
                vector.setId(result.getLong("id"));
                vector.setOutput(result.getString("output"));
                vector.setPoints(result.getDouble("points"));
                vector.setAssignmentId(result.getLong("assignment_id"));

                //arguments are stored as json string
                String argumentsJson = result.getString("arguments");

                if (argumentsJson != null && !argumentsJson.isEmpty()) {
                    Type type = new TypeToken<Collection<Argument>>() {
                    }.getType();

                    List<Argument> arguments = FormatUtil.fromJson(argumentsJson, type);
                    vector.setArguments(arguments);
                }

                vectors.add(vector);
            }
        }
        return vectors;
    }

    public static Vector insert(Vector vector, long assignmentId) throws SQLException, JsonSyntaxException {
        Vector created = null;
        long generatedId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, FormatUtil.json(vector.getArguments()));
            statement.setDouble(2, vector.getPoints());
            statement.setLong(3, assignmentId);
            statement.execute();
            ResultSet result = statement.getGeneratedKeys();

            if (result.next()) {
                generatedId = result.getLong(1);
            }
        }

        if (generatedId > -1) {
            created = getById(generatedId);
        }
        return created;
    }

    public static Vector updateOutputById(long id, String output) throws SQLException {
        Vector updated = null;
        long generatedId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_OUTPUT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, output);
            statement.setLong(2, id);

            statement.execute();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                generatedId = result.getLong(1);
            }
        }

        if (generatedId > -1) {
            updated = getById(generatedId);
        }
        return updated;
    }

    public static Vector update(Vector vector) throws SQLException {
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_QUERY)) {

            statement.setDouble(1, vector.getPoints());
            statement.setString(2, FormatUtil.json(vector.getArguments()));
            statement.setLong(3, vector.getId());
            statement.execute();
        }

        Vector updated = getById(vector.getId());

        //update assignment status to rerun with new vector
        if (updated != null) {
            AssignmentService.setUpdated(vector.getAssignmentId(), true);
        }

        return updated;
    }

    public static void delete(long vectorId) throws SQLException {

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(DELETE_QUERY)) {

            statement.setLong(1, vectorId);
            statement.execute();
        }
    }
    
    public static long countByAssignmentId(long assignmentId) throws SQLException {
        long count = 0;
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(COUNT_BY_ASSIGNMENT_ID_QUERY)) {
            statement.setLong(1, assignmentId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getLong(1);
            }
        }

        return count;
    }
}
