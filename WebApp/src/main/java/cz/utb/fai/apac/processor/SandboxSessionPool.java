/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.SandboxClient;
import io.docker.DockerException;
import io.docker.DockerNotRunningException;
import org.apache.commons.pool2.PoolUtils;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author František Špaček
 */
@Singleton
@Startup
@DependsOn("ConfigurationUtil")
public class SandboxSessionPool {

    private static GenericObjectPool<SandboxSession> sessionPool;
    private static final Logger LOGGER = LoggerFactory.getLogger(SandboxSessionPool.class);
    private static SandboxClient sandboxClient;

    @PostConstruct
    public void init() {
        LOGGER.info("Initializing sandbox pool");

        try {
            sandboxClient = new SandboxClient();
            if (isDockerRunning()) {
                sessionPool = new GenericObjectPool<>(new SandboxSessionFactory(sandboxClient));
                int sandboxCount = Integer.valueOf(ConfigurationUtil.getProperty("apac.sandboxCount"));

                //test if container ssh session is still alive
                sessionPool.setTestOnBorrow(true);

                //one container should be always available
                sessionPool.setMinIdle(1);
                sessionPool.setMinEvictableIdleTimeMillis(60000);

                sessionPool.setMaxTotal(sandboxCount * 2);

                PoolUtils.prefill(sessionPool, sandboxCount);
            } else {
                LOGGER.info("Docker is not running, please start it first");
            }
        } catch (DockerException ex) {
            LOGGER.error("Sandbox Pool Init", ex);
        } catch (Exception ex) {
            LOGGER.error("Sandbox Pool Init", ex);
        }

    }

    @PreDestroy
    public static void shutdown() {
        LOGGER.info("Closing all sessions in pool");
        sessionPool.close();
    }

    public static SandboxSession getSandbox() throws Exception {
        LOGGER.debug("Borrowing sandbox instance");
        return sessionPool.borrowObject();
    }

    public static void returnSandbox(SandboxSession session) {
        LOGGER.debug("Returning sandbox instance");
        sessionPool.returnObject(session);
    }

    public static int getIdleSandboxCount() {
        return sessionPool.getNumIdle();
    }

    public static boolean isDockerRunning() {
        boolean result = true;
        try {
            sandboxClient.getInfo();
        } catch (DockerException ex) {
            LOGGER.error("Docker ex", ex);
        } catch (DockerNotRunningException ex) {
            LOGGER.error("Docker is not running", ex);
            result = false;
        }

        return result;
    }

}
