/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Frantisek Spacek
 */
@Singleton
@Startup
public class ConfigurationUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationUtil.class);
    private static Properties properties;

    @PostConstruct
    public static void init() {
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
            properties = new Properties();
            properties.load(inputStream);
            System.out.println("libs: " + System.getProperty("java.library.path"));
        } catch (IOException ex) {
            LOGGER.error("Property Util Exception: ", ex);
        }
    }

    public static void initDebug(String configPath) {
        try {
            InputStream inputStream = new FileInputStream(configPath);
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException ex) {
            LOGGER.error("Property Util Exception: ", ex);
        }
    }

    public static Properties getProperties() {
        return properties;
    }

    public static String getProperty(String key) {
        return properties.containsKey(key) ? properties.getProperty(key) : "";
    }

}
