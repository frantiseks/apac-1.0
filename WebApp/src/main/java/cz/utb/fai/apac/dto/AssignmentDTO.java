/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.dto;

import cz.utb.fai.apac.entity.Assignment;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 *
 * @author František Špaček
 */
public class AssignmentDTO {

    private Long id;

    @NotNull
    @Size(min = 1)
    private final String language;

    @Min(0)
    @Max(100)
    private final int threshold;

    @Min(10000)
    @Max(300000)
    private final int maxRuntime;

    @Min(1)
    @Max(10)
    private final int maxSize;
    private final Date timestamp;

    @NotNull
    @Size(min = 1)
    private final String name;
    private final String description;
    private final String baseFile;

    public AssignmentDTO(Assignment assignment) {
        this.id = assignment.getId() > 0 ? assignment.getId() : null;
        this.language = assignment.getLanguage();
        this.threshold = assignment.getThreshold();
        this.maxRuntime = assignment.getMaxRuntime();
        this.maxSize = assignment.getMaxSize();
        this.timestamp = assignment.getTimestamp();
        this.name = assignment.getName();
        this.description = assignment.getDescription();
        this.baseFile = assignment.getBaseFile() != null ? assignment.getBaseFile().getName() : null;
    }

    public Assignment getAssignment() {
        Assignment assignment = new Assignment();

        if (id != null) {
            assignment.setId(id);
        }
        assignment.setLanguage(language);
        assignment.setMaxRuntime(maxRuntime);
        assignment.setMaxSize(maxSize);
        assignment.setThreshold(threshold);
        assignment.setName(name);
        assignment.setDescription(description);
        return assignment;
    }

    public Long getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }

    public int getThreshold() {
        return threshold;
    }

    public int getMaxRuntime() {
        return maxRuntime;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBaseFile() {
        return baseFile;
    }

    public boolean isValid() {
        boolean isValid = true;

        isValid &= language != null && !language.isEmpty();
        isValid &= threshold > 0 && threshold <= 100;
        isValid &= maxRuntime > 10000 && maxRuntime <= 300000;
        isValid &= maxSize >= 1 && maxSize <= 10;
        isValid &= name != null && !name.isEmpty();

        return isValid;
    }
}
