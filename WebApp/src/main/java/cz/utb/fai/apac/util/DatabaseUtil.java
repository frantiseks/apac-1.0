/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author František Špaček
 */
@Singleton
@Startup
@DependsOn("ConfigurationUtil")
public class DatabaseUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseUtil.class);
    private static DataSource dataSource;

    @PostConstruct
    public static void init() {
        try {
            InitialContext context = new InitialContext();
            LOGGER.info("Looking for DataSource JNDI");
            dataSource = (DataSource) context.lookup(ConfigurationUtil.getProperty("apac.dbJNDI"));
        } catch (NamingException ex) {
            LOGGER.error("DB Pool init", ex);
        }
    }

    public static void initDebug(InitialContext context) {
        try {
            dataSource = (DataSource) context.lookup(ConfigurationUtil.getProperty("apac.dbJNDI"));
        } catch (NamingException ex) {
            LOGGER.error("DB Pool init", ex);
        }
    }

    /**
     * Get connection from DataSource
     * @return 
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (SQLException ex) {
            LOGGER.error("Get DB Connection", ex);
        }

        return conn;
    }

}
