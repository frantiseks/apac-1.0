/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import io.docker.client.DockerClientImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author František Špaček
 */
public class SandboxClient extends DockerClientImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(SandboxClient.class);

    /**
     * Extended DockerClient as SandboxClient
     */
    public SandboxClient() {
        super("unix", "/var/run/docker.sock", 80);

        LOGGER.info("Creating SandboxClient instance");
    }
}
