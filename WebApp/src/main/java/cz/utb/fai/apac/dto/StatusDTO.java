/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.dto;

import io.docker.entity.Container;
import io.docker.entity.DockerInfo;
import io.docker.entity.DockerVersion;

import java.util.List;

/**
 *
 * @author František Špaček
 */
public class StatusDTO {

    DockerInfo dockerInfo;
    DockerVersion dockerVersion;
    List<Container> containers;
    List<String> processors;

    public DockerInfo getDockerInfo() {
        return dockerInfo;
    }

    public void setDockerInfo(DockerInfo dockerInfo) {
        this.dockerInfo = dockerInfo;
    }

    public DockerVersion getDockerVersion() {
        return dockerVersion;
    }

    public void setDockerVersion(DockerVersion dockerVersion) {
        this.dockerVersion = dockerVersion;
    }

    public List<Container> getContainers() {
        return containers;
    }

    public void setContainers(List<Container> containers) {
        this.containers = containers;
    }

    public List<String> getProcessors() {
        return processors;
    }

    public void setProcessors(List<String> processors) {
        this.processors = processors;
    }

    @Override
    public String toString() {
        return "Status{" + "dockerInfo=" + dockerInfo + ", dockerVersion=" + dockerVersion + ", containers=" + containers + ", processors=" + processors + '}';
    }
}
