/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.dto;

import cz.utb.fai.apac.entity.Execution;

import java.util.Date;

/**
 *
 * @author František Špaček
 */
public class ExecutionDTO {
    private final String output;
    private final VectorDTO vector;
    private final double points;
    private final boolean terminated;
    private final Date timestamp;

    public ExecutionDTO(Execution execution) {
        this.output = execution.getOutput();
        this.vector = new VectorDTO(execution.getVector());
        this.points = execution.getPoints();
        this.terminated = execution.isTerminated();
        this.timestamp = execution.getTimestamp();
    }

    public String getOutput() {
        return output;
    }

    public VectorDTO getVector() {
        return vector;
    }

    public double getPoints() {
        return points;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "ExecutionDTO{" + "output=" + output + ", vector=" + vector + ", "
                + "points=" + points + ", terminated=" + terminated + ", timestamp=" + timestamp + '}';
    }
    
    
    
    
}
