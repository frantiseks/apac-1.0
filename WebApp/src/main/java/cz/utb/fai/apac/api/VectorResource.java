/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.batch.ExecuteEvalution;
import cz.utb.fai.apac.database.AssignmentService;
import cz.utb.fai.apac.database.ExecutionService;
import cz.utb.fai.apac.database.VectorService;
import cz.utb.fai.apac.dto.VectorDTO;
import cz.utb.fai.apac.entity.Assignment;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.util.VectorUtil;
import cz.utb.fai.apac.util.ZipUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
@Path("vector")
@Produces(MediaType.APPLICATION_JSON)
public class VectorResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(VectorResource.class);

    @GET
    @Path("{vectorId}")
    public Response getVector(@Min(1) @PathParam("vectorId") long vectorId) throws SQLException {
        Response result = Response.status(Status.NOT_FOUND).build();

        Vector vector = VectorService.getById(vectorId);
        if (vector != null) {
            result = Response.ok(new VectorDTO(vector)).build();
        }

        return result;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response defineVector(@Min(1) @QueryParam("assignmentId") long assignmentId, @NotNull VectorDTO vector) throws SQLException, IOException {
        Response result = Response.serverError().build();

        Assignment assignment = AssignmentService.getById(assignmentId);
        if (assignment != null) {
            if (VectorUtil.validate(vector)) {
                if (assignment.getBaseFile() != null) {
                    Vector created = VectorService.insert(vector.toVector(), assignmentId);

                    if (VectorUtil.getSTDINFileName(created).isEmpty()) {
                        long jobId = ExecuteEvalution
                                .execute(assignment.getBaseFile(), new Submission().getWorkspaceUUID(), assignmentId, true, assignment.getBaseFile().getName());
                    }
                    result = Response.ok(new VectorDTO(created)).build();
                } else {
                    result = Response.status(Status.PRECONDITION_FAILED).build();
                }
            } else {
                result = Response.status(Status.BAD_REQUEST).build();
            }
        }

        return result;
    }

    @PUT
    @Path("{vectorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateVector(@Min(1) @PathParam("vectorId") long vectorId, @NotNull VectorDTO vector) throws SQLException, IOException {
        Response result = Response.serverError().build();

        Vector fetchedResult = VectorService.getById(vectorId);

        if (fetchedResult != null) {
            if (VectorUtil.validate(vector)) {
                Vector v = vector.toVector();
                v.setId(vectorId);
                v.setAssignmentId(fetchedResult.getAssignmentId());

                v = VectorService.update(v);

                if (v != null) {
                    Assignment assignment = AssignmentService.getById(v.getAssignmentId());
                    long jobId = ExecuteEvalution
                            .execute(assignment.getBaseFile(), new Submission().getWorkspaceUUID(), assignment.getId(), true, assignment.getBaseFile().getName());
                    result = Response.ok(new VectorDTO(v)).build();
                }
            } else {
                result = Response.status(Status.BAD_REQUEST).build();
            }

        } else {
            result = Response.status(Status.NOT_FOUND).build();
        }

        return result;
    }

    @DELETE
    @Path("{vectorId}")
    public Response deleteVector(@Min(1) @PathParam("vectorId") long vectorId)
            throws SQLException, IOException {
        Response result = Response.status(Status.NOT_FOUND).build();

        Vector fetchedResult = VectorService.getById(vectorId);

        if (fetchedResult != null) {

            if (ExecutionService.countByVectorId(vectorId) == 0) {
                VectorService.delete(vectorId);

                Assignment assignment = AssignmentService.getById(fetchedResult.getAssignmentId());
                long jobId = ExecuteEvalution
                        .execute(assignment.getBaseFile(), new Submission().getWorkspaceUUID(), assignment.getId(), true, assignment.getBaseFile().getName());
                result = Response.ok().build();
            } else {
                result = Response.status(Status.CONFLICT).build();
            }
        }

        return result;
    }

    @GET
    public Response getVectorsByAssignmentId(@Min(1) @QueryParam("assignmentId") long assignmentId)
            throws SQLException {
        List<Vector> fetchedResults = VectorService.getAllByAssignmentId(assignmentId);

        List<VectorDTO> vectors = new ArrayList<>();

        fetchedResults.stream().forEach((v) -> {
            vectors.add(new VectorDTO(v));
        });

        return Response.ok().entity(vectors).build();

    }

    @POST
    @Path("{vectorId}/upload")
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    public Response uploadFile(@Min(1) @PathParam("vectorId") long vectorId, @NotNull @Size(min = 1) @QueryParam("filename") String filename, @NotNull File file)
            throws IOException, SQLException {
        Response result = Response.serverError().build();

        if (file.length() == 0 || ZipUtil.isZip(file)) {
            result = Response.status(Status.BAD_REQUEST).build();
        } else {

            Vector vector = VectorService.getById(vectorId);

            if (vector != null) {
                if (VectorUtil.validateSTDINFile(vector.getArguments(), filename)) {
                    Assignment assignment = AssignmentService.getById(vector.getAssignmentId());
                    if (assignment != null) {

                        if (!VectorUtil.getSTDINFileName(vector).isEmpty()) {
                            new File(assignment.getBaseFile().getParentFile() + File.separator + VectorUtil.getSTDINFileName(vector)).delete();
                        }
                        FileUtils.copyFile(file, new File(assignment.getBaseFile().getParentFile() + File.separator + filename));

                        long jobId = ExecuteEvalution.execute(assignment.getBaseFile(), new Submission().getWorkspaceUUID(), assignment.getId(), true, assignment.getBaseFile().getName());
                        result = Response.ok().build();
                    }
                } else {
                    result = Response.status(Status.METHOD_NOT_ALLOWED).build();
                }
            } else {
                result = Response.status(Status.NOT_FOUND).build();
            }
        }
        return result;
    }
}
