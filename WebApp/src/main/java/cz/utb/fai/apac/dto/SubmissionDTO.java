/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.dto;

import cz.utb.fai.apac.entity.Submission;

import java.util.Date;

/**
 *
 * @author František Špaček
 */
public class SubmissionDTO {

    private final Long timestamp;
    private final Double totalPoints;
    private final String compilerOutput;
    private final Boolean compileFailed;
    private final String uuid;
    private Long jobId;
    private final Boolean reference;
    private final AssignmentDTO assignment;

    public SubmissionDTO(Submission submission) {
        if (submission.getTimestamp() != null) {
            this.timestamp = submission.getTimestamp().getTime();
        } else {
            this.timestamp = null;
        }
        this.totalPoints = submission.getTotalPoints();
        this.compilerOutput = submission.getCompilerOutput();
        this.compileFailed = submission.isCompileFailed();
        this.uuid = submission.getWorkspaceUUID();
        this.reference = submission.isReference();

        if (submission.getAssignment() != null) {
            this.assignment = new AssignmentDTO(submission.getAssignment());
        } else {
            this.assignment = null;
        }
    }
    public SubmissionDTO(Submission submission, boolean reference) {
        if (submission.getTimestamp() != null) {
            this.timestamp = submission.getTimestamp().getTime();
        } else {
            this.timestamp = null;
        }
        this.totalPoints = null;
        this.compilerOutput = null;
        this.compileFailed = null;
        this.uuid = submission.getWorkspaceUUID();
        this.reference = submission.isReference();

        if (submission.getAssignment() != null) {
            this.assignment = new AssignmentDTO(submission.getAssignment());
        } else {
            this.assignment = null;
        }
    }

    public Submission toSubmission() {
        Submission submission = new Submission();
        submission.setTimestamp(new Date(timestamp));
        submission.setTotalPoints(totalPoints);
        submission.setCompilerOutput(compilerOutput);
        submission.setCompileFailed(compileFailed);
        submission.setWorkspaceUUID(uuid);

        return submission;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public String getCompilerOutput() {
        return compilerOutput;
    }

    public boolean isCompileFailed() {
        return compileFailed;
    }

    public String getUuid() {
        return uuid;
    }

    public boolean isReference() {
        return reference;
    }

    public AssignmentDTO getAssignment() {
        return assignment;
    }

}
