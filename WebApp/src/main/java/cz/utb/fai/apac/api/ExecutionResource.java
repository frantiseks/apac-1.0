/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.database.ExecutionService;
import cz.utb.fai.apac.database.SubmissionService;
import cz.utb.fai.apac.database.VectorService;
import cz.utb.fai.apac.dto.ExecutionDTO;
import cz.utb.fai.apac.entity.Execution;
import cz.utb.fai.apac.entity.Submission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
@Path("execution")
@Produces(MediaType.APPLICATION_JSON)
public class ExecutionResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionResource.class);

    @GET
    public Response getExecutionBySubmissionUUID(@NotNull @Size(min = 32) @QueryParam("uuid") String uuid) throws SQLException {
        Response result = Response.serverError().build();

        Submission submission = SubmissionService.getByUUID(uuid);

        if (submission == null) {
            result = Response.status(Status.NOT_FOUND).build();
        } else {

            List<Execution> fetchedResults = ExecutionService.getAllBySubmissionId(submission.getId());

            List<ExecutionDTO> executions = new ArrayList<>();

            for (Execution e : fetchedResults) {
                e.setVector(VectorService.getById(e.getVectorId()));
                executions.add(new ExecutionDTO(e));
            }

            result = Response.ok().entity(executions).build();
        }

        return result;
    }
}
