/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import io.docker.entity.ContainerDetail;

import java.util.Properties;

/**
 *
 * @author František Špaček
 */
public final class PropertiesBuilder {

    private static final int SSH_PORT = Integer.valueOf(ConfigurationUtil.getProperty("apac.containerSSHPort"));
    private static final String EXPORT_DATA_DIR = "/root";

    /**
     * Build default connection properties for SSH session with container
     *
     * @param containerDetail
     * @return
     */
    public static Properties buildDefault(ContainerDetail containerDetail) {
        return buildProperties(containerDetail);
    }

    /**
     * Build connection properties for SSH session with container
     *
     * @param containerDetail
     * @return
     */
    private static Properties buildProperties(ContainerDetail containerDetail) {
        Properties properties = new Properties();
        properties.setProperty("user", ConfigurationUtil.getProperty("apac.containerSSHUser"));
        properties.setProperty("password", ConfigurationUtil.getProperty("apac.containerSSHPassword"));
        properties.setProperty("hostname", containerDetail.getNetworkSettings().getIpAddress());
        properties.setProperty("port", String.valueOf(SSH_PORT));

        properties.setProperty("StrictHostKeyChecking", "no");
        properties.setProperty("exportDir", EXPORT_DATA_DIR);
        return properties;
    }

}
