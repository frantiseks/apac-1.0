/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.batch;

import cz.utb.fai.apac.api.AssignmentResource;
import cz.utb.fai.apac.database.AssignmentService;
import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.ZipUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.zip.ZipException;

/**
 *
 * @author František Špaček
 */
public final class ExecuteEvalution {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssignmentResource.class);
    private static final String APAC_TEMP_DIR = ConfigurationUtil.getProperty("apac.tempDir");

    /**
     *
     * @param submissionFile - file with solution
     * @param workspaceUUID - workspace name
     * @param assignmentId - assignment id
     * @param filename - name of submitted file
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public static long execute(File submissionFile, String workspaceUUID, long assignmentId, String filename) throws IOException, SQLException {
        return execute(submissionFile, workspaceUUID, assignmentId, false, filename);
    }

    /**
     *
     * @param submissionFile - file with solution
     * @param workspaceUUID - workspace name
     * @param assignmentId - assignment id
     * @param reference - flag for reference submission
     * @param filename - name of submitted file
     * @return
     * @throws IOException
     * @throws SQLException
     * @throws java.util.zip.ZipException
     */
    public static long execute(File submissionFile, String workspaceUUID, long assignmentId, boolean reference, String filename) throws IOException, SQLException, ZipException {

        long jobId = -1;
        //submission workspace path
        File uploadedFile = new File(String
                .format("%s%s%s%s", APAC_TEMP_DIR, workspaceUUID, File.separator, filename.isEmpty() ? new Date().getTime() : filename));

        //create workspace folder
        if (uploadedFile.getParentFile().mkdir()) {

            //save uploaded stream to filesystem
            FileUtils.copyFile(submissionFile, uploadedFile);
            if (ZipUtil.isZip(uploadedFile)) {

                LOGGER.debug("Unziping file {}", uploadedFile.getAbsolutePath());
                ZipUtil.unzip(uploadedFile, uploadedFile.getParentFile());

                LOGGER.debug("Deleting uploaded file {}", uploadedFile.getAbsolutePath());
                FileUtils.deleteQuietly(uploadedFile);
            }

            JobOperator jobOperator = BatchRuntime.getJobOperator();

            //configure job properties
            Properties jobProperties = new Properties();
            jobProperties.setProperty("submissionUUID", workspaceUUID);
            jobProperties.setProperty("assignmentId", String.valueOf(assignmentId));
            jobProperties.setProperty("referenceSubmission", String.valueOf(reference));

            //start batch job
            jobId = jobOperator.start("apacJob", jobProperties);

            //if reference submission update base file for assignment
            if (reference) {
                AssignmentService.updateBaseFile(assignmentId, submissionFile.getAbsolutePath());
            }
        }

        return jobId;
    }
}
