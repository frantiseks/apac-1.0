/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.database;

import cz.utb.fai.apac.entity.Execution;
import cz.utb.fai.apac.util.DatabaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class ExecutionService {

    private static final String SELECT_ALL_BY_SUBMISSION_ID_QUERY = "SELECT * FROM executions WHERE submission_id = ?";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM executions WHERE id = ?";
    private static final String INSERT_QUERY = "INSERT INTO executions (submission_id, points, output, execution_terminated, vector_id) VALUES (?, ?, ?, ?, ?)";
    private static final String COUNT_BY_VECTOR_ID_QUERY = "SELECT COUNT(*) FROM executions WHERE vector_id = ?";

    public static List<Execution> getAllBySubmissionId(long id) throws SQLException {
        List<Execution> executions = new ArrayList<>();
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_BY_SUBMISSION_ID_QUERY)) {

            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Execution execution = new Execution();
                execution.setId(result.getLong("id"));
                execution.setSubmissionId(result.getLong("submission_id"));
                execution.setOutput(result.getString("output"));
                execution.setPoints(result.getDouble("points"));
                execution.setTerminated(result.getBoolean("execution_terminated"));
                execution.setTimestamp(result.getTimestamp("timestamp"));
                execution.setVectorId(result.getLong("vector_id"));

                executions.add(execution);
            }
        }

        for (Execution e : executions) {
            e.setVector(VectorService.getById(e.getId()));
        }

        return executions;
    }

    public static Execution getById(long id) throws SQLException {
        Execution execution = null;
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_BY_ID_QUERY)) {

            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                execution = new Execution();
                execution.setId(result.getLong("id"));
                execution.setSubmissionId(result.getLong("submission_id"));
                execution.setOutput(result.getString("output"));
                execution.setPoints(result.getDouble("points"));
                execution.setTerminated(result.getBoolean("execution_terminated"));
                execution.setTimestamp(result.getTimestamp("timestamp"));
                execution.setVectorId(result.getLong("vector_id"));
            }
        }

        return execution;
    }

    public static Execution insert(Execution execution) throws SQLException {

        Execution created = null;
        long generatedId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            statement.setLong(1, execution.getSubmissionId());
            statement.setDouble(2, execution.getPoints());
            statement.setString(3, execution.getOutput());
            statement.setBoolean(4, execution.isTerminated());
            statement.setLong(5, execution.getVectorId());

            statement.execute();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                generatedId = result.getLong(1);
            }
        }

        if (generatedId > -1) {
            created = getById(generatedId);
        }

        return created;
    }

    public static long countByVectorId(long vectorId) throws SQLException {
        long count = 0;
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(COUNT_BY_VECTOR_ID_QUERY)) {
            statement.setLong(1, vectorId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getLong(1);
            }
        }

        return count;
    }
}
