/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.processor;

import com.google.gson.Gson;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.PropertiesBuilder;
import cz.utb.fai.apac.util.SandboxClient;
import cz.utb.fai.apac.util.SandboxUtil;
import io.docker.DockerException;
import io.docker.DockerNotRunningException;
import io.docker.entity.*;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

/**
 *
 * @author František Špaček
 */
public class SandboxSessionFactory extends BasePooledObjectFactory<SandboxSession> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SandboxSessionFactory.class);
    private static final String DEFAULT_CONTAINER_CONFIG = String
            .format("{\"Image\":\"%s\",\"Memory\":%s,\"CpuShares\":%s,\"Tty\":true}",
                    ConfigurationUtil.getProperty("apac.containerImage"),
                    ConfigurationUtil.getProperty("apac.containerMemory"),
                    ConfigurationUtil.getProperty("apac.containerCPU"));

    private static final String IMAGE_NAME = ConfigurationUtil.getProperty("apac.containerImage");
    private static final int SSH_PORT = Integer.valueOf(ConfigurationUtil.getProperty("apac.containerSSHPort"));
    private static final int MAX_PROCESSES = Integer.valueOf(ConfigurationUtil.getProperty("apac.containerMaxProcesses"));
    private static final String PS_ARGS = "aux";

    private final JSch jschClient = new JSch();

    private final SandboxClient sandboxClient;

    private final List<Container> existingContainers;

    public SandboxSessionFactory(final SandboxClient sandboxClient) throws DockerException, DockerNotRunningException {
        this.sandboxClient = sandboxClient;
        existingContainers = getOnlySSHRunningContainers();
        LOGGER.info(String.format("Available SSH Sandbox Containers: %s", existingContainers.size()));
    }

    @Override
    public SandboxSession create() throws Exception {

        ContainerDetail containerDetail;
        //if some sandbox containers exists use them too
        if (!existingContainers.isEmpty()) {
            LOGGER.info("Using existing container");
            containerDetail = sandboxClient.inspectContainer(existingContainers.remove(0).getId());
        } else {
            LOGGER.info("Creating new container");
            containerDetail = createAndStartDefaultContainer();
        }

        Properties connectionProperties = PropertiesBuilder.buildDefault(containerDetail);
        String user = connectionProperties.getProperty("user");
        String hostname = connectionProperties.getProperty("hostname");
        int port = Integer.valueOf(connectionProperties.getProperty("port"));
        String password = connectionProperties.getProperty("password");
        Session sshSession = jschClient.getSession(user, hostname, port);
        sshSession.setPassword(password);
        sshSession.setConfig(connectionProperties);
        sshSession.connect();

        return new SandboxSession(containerDetail.getId(), sshSession);
    }

    @Override
    public PooledObject<SandboxSession> wrap(SandboxSession obj) {
        return new DefaultPooledObject<>(obj);
    }

    @Override
    public void destroyObject(PooledObject<SandboxSession> p) throws Exception {
        LOGGER.info("Destroying session with container {}", p.getObject().getContainerId());
        p.getObject().getSshSession().disconnect();

        LOGGER.info("Deleting container {}", p.getObject().getContainerId());
        sandboxClient.deleteContainer(p.getObject().getContainerId());
    }

    @Override
    public boolean validateObject(PooledObject<SandboxSession> p) {

        boolean isValid = true;

        isValid &= p.getObject().getContainerId() != null;

        if (isValid) {
            try {
                List<String[]> processes = sandboxClient.getContainerProcesses(p.getObject().getContainerId(), PS_ARGS).getProcesses();
                isValid &= processes.size() <= MAX_PROCESSES;

            } catch (DockerException | DockerNotRunningException ex) {
                java.util.logging.Logger.getLogger(SandboxSessionFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
            LOGGER.info("Validating ssh session");
            isValid &= SandboxUtil.isSessionAlive(p.getObject());
        }

        return isValid;
    }

    private List<Container> getOnlySSHRunningContainers() throws DockerException, DockerNotRunningException {
        List<Container> containers = sandboxClient.getContainers(false);
        System.out.println(containers);

        List<Container> sshContainers = new ArrayList<>();

        for (Container c : containers) {
            String imageName = c.getImage();

            if (!imageName.contains(IMAGE_NAME)) {
                continue;
            }

            Port[] ports = c.getPorts();

            for (Port p : ports) {
                if (p.getPublicPort() == SSH_PORT) {
                    sshContainers.add(c);
                    break;
                }
            }
        }

        return sshContainers;
    }

    private ContainerDetail createAndStartDefaultContainer() throws DockerException, DockerNotRunningException {
        String id = createContainer(DEFAULT_CONTAINER_CONFIG);
        startContainer(id);

        //sleep thread, to give to docker time for initialization of network
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            LOGGER.error("Dealing ssh connection exception", ex);
        }
        return sandboxClient.inspectContainer(id);
    }

    private String createContainer(String parameters) throws DockerException, DockerNotRunningException {
        ContainerInfo createdContainer = sandboxClient.createContainer(
                new Gson().fromJson(parameters, ContainerConfig.class));

        return createdContainer.getId();
    }

    private void startContainer(String containerId) throws DockerException, DockerNotRunningException {
        sandboxClient.startContainer(containerId);
    }

}
