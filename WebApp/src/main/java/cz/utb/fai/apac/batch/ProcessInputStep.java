/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.batch;

import cz.utb.fai.apac.database.AssignmentService;
import cz.utb.fai.apac.entity.Assignment;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.util.ConfigurationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.api.AbstractBatchlet;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.Properties;

/**
 *
 * @author Frantisek Spacek
 */
@Named
public class ProcessInputStep extends AbstractBatchlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessInputStep.class);
    @Inject
    private JobContext jobContext;

    @Override
    public String process() throws Exception {

        LOGGER.debug("Processing input");
        String exitStatus = String.valueOf(ExitStatus.INPUT_NOT_PROCESSED);
        jobContext.setExitStatus(exitStatus);

        Properties jobProperties = BatchRuntime.getJobOperator().getParameters(jobContext.getExecutionId());
        String submissionUUID = jobProperties.getProperty("submissionUUID");
        int assignmentId = Integer.valueOf(jobProperties.getProperty("assignmentId"));

        String apacTemp = ConfigurationUtil.getProperty("apac.tempDir");

        //submission workspace path
        File submissionWorkspace = new File(String.format("%s%s%s", apacTemp, File.separator, submissionUUID));

        //fetch assignment configuration from database
        Assignment assignment = AssignmentService.getById(assignmentId);
        
        Submission submission = new Submission(submissionUUID);
        submission.setAssignment(assignment);
        submission.setSubmissionWorkspace(submissionWorkspace);

        jobContext.setTransientUserData(submission);

        exitStatus = String.valueOf(ExitStatus.INPUT_PROCESSED);

        return exitStatus;
    }

}
