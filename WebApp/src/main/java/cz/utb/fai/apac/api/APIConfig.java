/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.mapper.*;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author František Špaček
 */
@ApplicationPath("api")
public class APIConfig extends Application {

    private final Set<Object> singletons = new HashSet<>();
    private final Set<Class<?>> classes = new HashSet<>();

    public APIConfig() {
        //extends rest application with custom providers
        singletons.add(new GsonProvider());
        singletons.add(new NullPointerExceptionMapper());
        singletons.add(new JsonSyntaxExceptionMapper());
        singletons.add(new NotFoundExceptionMapper());
        singletons.add(new ForbiddenExceptionMapper());
        singletons.add(new DockerExceptionMapper());
        singletons.add(new DockerNotFoundExceptionMapper());
        singletons.add(new DockerNotRunningExceptionMapper());
        singletons.add(new SQLExceptionMapper());

        //register rest resources
        classes.add(SubmissionResource.class);
        classes.add(AssignmentResource.class);
        classes.add(AdminResource.class);
        classes.add(TaskResource.class);
        classes.add(VectorResource.class);
        classes.add(ExecutionResource.class);
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

}
