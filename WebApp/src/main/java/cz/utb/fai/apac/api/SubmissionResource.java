/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.batch.ExecuteEvalution;
import cz.utb.fai.apac.database.AssignmentService;
import cz.utb.fai.apac.database.SubmissionService;
import cz.utb.fai.apac.database.VectorService;
import cz.utb.fai.apac.dto.SubmissionDTO;
import cz.utb.fai.apac.entity.Assignment;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.util.ZipUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frantisek Spacek
 */
@Path("submission")
@Produces(MediaType.APPLICATION_JSON)
public class SubmissionResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubmissionResource.class);

    @GET
    public Response getSubmissions(@QueryParam("assignmentId") Long id,
            @QueryParam("days") Integer days, @QueryParam("from") Long from, @QueryParam("to") Long to) throws SQLException {
        List<Submission> submissions = new ArrayList<>();

        //submission filters
        if (days != null) {
            submissions.addAll(SubmissionService.getLatest(days));
        } else if (id != null && from != null && to != null) {
            submissions.addAll(SubmissionService.getSubmissionsByFilter(id, from, to));
        } else if (id != null) {
            submissions.addAll(SubmissionService.getAllByAssigmentId(id));
        }

        List<SubmissionDTO> dtoList = new ArrayList<>();

        for (Submission s : submissions) {
            s.setAssignment(AssignmentService.getById(s.getAssignmentId()));
            dtoList.add(new SubmissionDTO(s));
        }

        return Response.ok(dtoList).build();
    }

    @GET
    @Path("{uuid}")
    public Response getSubmission(@NotNull @PathParam("uuid") String uuid) throws SQLException {
        Response result = Response.status(Status.NOT_FOUND).build();

        Submission submission = SubmissionService.getByUUID(uuid);

        if (submission != null) {
            result = Response.ok().entity(new SubmissionDTO(submission)).build();
        }

        return result;
    }

    @POST
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    public Response createSubmission(@NotNull File file, @Min(1) @QueryParam("assignmentId") long id, @QueryParam("filename") String filename) 
            throws SQLException, IOException {
        Response result = Response.status(Status.NOT_FOUND).build();

        Assignment assignment = AssignmentService.getById(id);
        List<Vector> vectors = VectorService.getAllByAssignmentId(id);

        if (assignment != null) {
            if ((file.length() / 1048576) > assignment.getMaxSize()) {
                result = Response.status(Status.NOT_ACCEPTABLE).build();

            } else if (vectors.isEmpty() || assignment.getBaseFile() == null) {

                //assignment does not have base file or vectors are not defined, submission is not allowed
                result = Response.status(Status.PRECONDITION_FAILED).build();

            } else {
                if (filename == null) {
                    filename = "";
                }

                if (!ZipUtil.isZip(file) && filename.isEmpty()) {
                    result = Response.status(Status.BAD_REQUEST).build();
                } else {
                    Submission submission = new Submission();
                    long jobId = ExecuteEvalution.execute(file, submission.getWorkspaceUUID(), id, filename);

                    SubmissionDTO dto = new SubmissionDTO(submission);
                    dto.setJobId(jobId);

                    result = jobId > -1
                            ? Response.ok().entity(dto).build()
                            : Response.status(Status.NOT_ACCEPTABLE).build();
                }
            }
        }
        return result;
    }

    @PUT
    @Path("{uuid}")
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    public Response updateSubmission(@NotNull File file, @NotNull @Size(min = 32) @PathParam("{uuid}") String uuid, @QueryParam("filename") String filename) 
            throws SQLException, IOException {
        Response result = Response.serverError().build();

        Submission submission = SubmissionService.getByUUID(uuid);

        if (submission == null) {
            return Response.status(Status.NOT_FOUND).build();
        }

        Assignment assignment = AssignmentService.getById(submission.getAssignmentId());
        List<Vector> vectors = VectorService.getAllByAssignmentId(submission.getAssignmentId());

        if (assignment != null) {
            //test if file has correct size
            if ((file.length() / 1048576) > assignment.getMaxSize()) {
                result = Response.status(Status.NOT_ACCEPTABLE).build();
            } else if (vectors.isEmpty() || assignment.getBaseFile() == null) {

                //assignment does not have base file or vectors are not defined, submission is not allowed
                result = Response.status(Status.METHOD_NOT_ALLOWED).build();
            } else {
                if (filename == null) {
                    filename = "";
                }

                if (!ZipUtil.isZip(file) && filename.isEmpty()) {
                    result = Response.status(Status.BAD_REQUEST).build();
                } else {
                    long jobId = ExecuteEvalution.execute(file, submission.getWorkspaceUUID(), assignment.getId(), filename);
                    SubmissionDTO dto = new SubmissionDTO(submission);
                    dto.setJobId(jobId);

                    result = jobId > -1
                            ? Response.ok().entity(dto).build()
                            : Response.status(Status.NOT_ACCEPTABLE).build();
                }
            }
        }
        return result;
    }
}
