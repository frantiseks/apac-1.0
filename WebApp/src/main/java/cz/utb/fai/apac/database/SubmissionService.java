/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.database;

import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.util.DatabaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class SubmissionService {

    private static final String SELECT_ALL_BY_ASSIGNMENT_ID_QUERY = "SELECT * FROM submissions WHERE assignment_id = ?";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM submissions WHERE id = ?";
    private static final String SELECT_BY_UUID_QUERY = "SELECT * FROM submissions WHERE workspace_uuid = ?";
    private static final String INSERT_QUERY = "INSERT INTO submissions (assignment_id, workspace_uuid, total_points, compiler_output, compile_failed,is_reference) VALUES (?, ?, ?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE submissions SET assignment_id=?, workspace_uuid=?, total_points=?, timestamp = now(), compiler_output=?, compile_failed=?, is_reference=? WHERE id = ?";
    private static final String GET_OLDER_THAN_QUERY = "SELECT * FROM submissions WHERE timestamp < (NOW() - INTERVAL ? DAY)";
    private static final String GET_LATEST_QEURY = "SELECT * FROM submissions WHERE timestamp > (NOW() - INTERVAL ? DAY) ORDER BY timestamp DESC";
    private static final String COUNT_SUBMISSIONS_BY_ASSIGNMENT_ID_QUERY = "SELECT count(*) FROM submissions WHERE assignment_id=?";
    private static final String SELECT_ALL_BY_ASSIGNMENT_ID_AND_REFERENCE_QUERY = "SELECT * FROM submissions WHERE assignment_id = ? AND is_reference = true";
    private static final String SELECT_ALL_BY_ASSIGNMENT_ID_AND_FILTER_QUERY = "SELECT * FROM submissions WHERE assignment_id = ? AND timestamp >= ? AND timestamp <= ? ORDER BY timestamp DESC";

    public static Submission getById(long id) throws SQLException {
        Submission submission = null;
        long assignmentId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_BY_ID_QUERY)) {

            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setReference(result.getBoolean("is_reference"));

                assignmentId = result.getLong("assignment_id");
            }
        }

        if (submission != null) {
            submission.setAssignment(AssignmentService.getById(assignmentId));
        }

        return submission;
    }

    public static Submission getByUUID(String uuid) throws SQLException {
        Submission submission = null;
        long assignmentId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_BY_UUID_QUERY)) {

            statement.setString(1, uuid);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setReference(result.getBoolean("is_reference"));
            }
        }

        if (submission != null) {
            submission.setAssignment(AssignmentService.getById(assignmentId));
        }

        return submission;
    }

    public static List<Submission> getAllByAssigmentId(long assignmentId) throws SQLException {
        List<Submission> submissions = new ArrayList<>();

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_BY_ASSIGNMENT_ID_QUERY)) {

            statement.setLong(1, assignmentId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Submission submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setAssignmentId(result.getLong("assignment_id"));
                submission.setReference(result.getBoolean("is_reference"));
                submissions.add(submission);
            }
        }

        return submissions;
    }

    public static Submission insert(Submission submission) throws SQLException {
        Submission created = null;
        int generatedId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            statement.setLong(1, submission.getAssignment().getId());
            statement.setString(2, submission.getWorkspaceUUID());
            statement.setDouble(3, submission.getTotalPoints() == null ? 0.0f : submission.getTotalPoints());
            statement.setString(4, submission.getCompilerOutput() == null ? "" : submission.getCompilerOutput());
            statement.setBoolean(5, submission.isCompileFailed() == null ? false : submission.isCompileFailed());
            statement.setBoolean(6, submission.isReference() == null ? false : submission.isReference());

            statement.execute();
            ResultSet result = statement.getGeneratedKeys();

            if (result.next()) {
                generatedId = result.getInt(1);
            }
        }

        if (generatedId > -1) {
            created = getById(generatedId);
        }

        return created;
    }

    public static Submission update(Submission submission) throws SQLException {
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            statement.setLong(1, submission.getAssignment().getId());
            statement.setString(2, submission.getWorkspaceUUID());
            statement.setDouble(3, submission.getTotalPoints() == null ? 0.0f : submission.getTotalPoints());
            statement.setString(4, submission.getCompilerOutput() == null ? "" : submission.getCompilerOutput());
            statement.setBoolean(5, submission.isCompileFailed() == null ? false : submission.isCompileFailed());
            statement.setBoolean(6, submission.isReference() == null ? false : submission.isReference());
            statement.setLong(7, submission.getId());

            statement.execute();
        }

        return getById(submission.getId());
    }

    public static List<Submission> getOlderThan(int days) throws SQLException {
        List<Submission> submissions = new ArrayList<>();

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(GET_OLDER_THAN_QUERY)) {

            statement.setInt(1, days);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Submission submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setReference(result.getBoolean("is_reference"));
                submissions.add(submission);
            }
        }
        return submissions;
    }

    public static List<Submission> getLatest(int days) throws SQLException {
        List<Submission> submissions = new ArrayList<>();

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(GET_LATEST_QEURY)) {

            statement.setInt(1, days);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Submission submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setAssignmentId(result.getLong("assignment_id"));
                submission.setReference(result.getBoolean("is_reference"));
                submissions.add(submission);
            }
        }
        return submissions;
    }

    public static long countByAssignmentId(long assignmentId) throws SQLException {
        long count = 0;
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(COUNT_SUBMISSIONS_BY_ASSIGNMENT_ID_QUERY)) {
            statement.setLong(1, assignmentId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                count = result.getLong(1);
            }
        }

        return count;
    }

    public static List<Submission> getReferenceSubmissions(long assignmentId) throws SQLException {
        List<Submission> submissions = new ArrayList<>();

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_BY_ASSIGNMENT_ID_AND_REFERENCE_QUERY)) {

            statement.setLong(1, assignmentId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Submission submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setAssignmentId(result.getLong("assignment_id"));
                submission.setReference(result.getBoolean("is_reference"));
                submissions.add(submission);
            }
        }
        return submissions;
    }

    public static List<Submission> getSubmissionsByFilter(long assignmentId, long from, long to) throws SQLException {
        List<Submission> submissions = new ArrayList<>();
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_BY_ASSIGNMENT_ID_AND_FILTER_QUERY)) {

            statement.setLong(1, assignmentId);
            statement.setTimestamp(2, new Timestamp(from));
            statement.setTimestamp(3, new Timestamp(to));
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Submission submission = new Submission();
                submission.setId(result.getLong("id"));
                submission.setWorkspaceUUID(result.getString("workspace_uuid"));
                submission.setTimestamp(result.getTimestamp("timestamp"));
                submission.setTotalPoints(result.getDouble("total_points"));
                submission.setCompilerOutput(result.getString("compiler_output"));
                submission.setCompileFailed(result.getBoolean("compile_failed"));
                submission.setAssignmentId(result.getLong("assignment_id"));
                submission.setReference(result.getBoolean("is_reference"));
                submissions.add(submission);
            }
        }
        return submissions;
    }
}
