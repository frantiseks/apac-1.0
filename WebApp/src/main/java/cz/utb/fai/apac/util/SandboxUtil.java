/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cz.utb.fai.apac.exception.SandboxException;
import cz.utb.fai.apac.processor.SandboxSession;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author František Špaček
 */
public final class SandboxUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SandboxUtil.class);
    private static final String SANDBOX_CONTAINER_DIR = "/home/sandbox";

    /**
     * put files to container over SFTP protocol
     *
     * @param sandboxSession
     * @param files
     * @throws JSchException
     * @throws SftpException
     * @throws Exception
     */
    public static void putFiles(SandboxSession sandboxSession, List<File> files) throws JSchException, SftpException, Exception {
        ChannelSftp channelSftp = (ChannelSftp) sandboxSession.getSshSession().openChannel("sftp");
        channelSftp.connect();

        try {
            for (File file : files) {
                if (file.exists() && channelSftp.isConnected()) {
                    if (file.isDirectory()) {
                        LOGGER.info(String.format("Directory %s found", file.getAbsolutePath()));
                        LOGGER.info(String.format("Starting to pick up all files from dir %s", file.getAbsolutePath()));

                    } else {
                        LOGGER.info(String.format("File %s was found", file.getAbsolutePath()));
                        LOGGER.info(String.format("Trying to move to dir %s in container for export", SANDBOX_CONTAINER_DIR));
                        channelSftp.cd(SANDBOX_CONTAINER_DIR);
                        channelSftp.put(new FileInputStream(file), file.getName());
                    }
                }
            }
        } finally {
            channelSftp.disconnect();
        }
    }

    /**
     * Execute command in container and fetch the results
     *
     * @param sandboxSession
     * @param command
     * @param maxExecutionRuntime
     * @return execution output
     * @throws JSchException
     * @throws SandboxException
     * @throws Exception
     */
    public static String executeCommand(SandboxSession sandboxSession, String command, long maxExecutionRuntime) throws JSchException, SandboxException, Exception {
        String result = "";

        ChannelExec channelExec = (ChannelExec) sandboxSession.getSshSession().openChannel("exec");

        InputStream stdout = channelExec.getInputStream();
        InputStream stderr = channelExec.getErrStream();

        channelExec.setCommand(command);
        channelExec.setPty(true);
        channelExec.connect();

        long startTime = new Date().getTime();
        boolean executionStopped = false;

        while (channelExec.getExitStatus() == -1) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                LOGGER.error("executeCommand ex", e);
            }

            //if process is running more than a maxExecutionRuntime
            if (new Date().getTime() > (startTime + maxExecutionRuntime)) {
                channelExec.sendSignal("INT");
                LOGGER.info("Execution stopped");

                executionStopped = true;
                break;
            }
        }

        if (executionStopped) {
            result = "Execution has been killed";
        } else {
            LOGGER.info("Channel exit status: " + channelExec.getExitStatus());

            if (channelExec.getExitStatus() != 0) {
                throw new SandboxException(IOUtils.toString(stderr));
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                Scanner scanner = new Scanner(stdout);

                while (scanner.hasNextLine()) {
                    stringBuilder.append(scanner.nextLine());
                    stringBuilder.append('\n');
                }

                result = stringBuilder.toString();
            }
        }

        channelExec.disconnect();
        return result;
    }

    /**
     * Test if SSH session is still alive
     *
     * @param session
     * @return
     */
    public static boolean isSessionAlive(SandboxSession session) {
        try {
            ChannelSftp channelSftp = (ChannelSftp) session.getSshSession().openChannel("sftp");
            channelSftp.connect();
            channelSftp.disconnect();
        } catch (JSchException ex) {
            LOGGER.info("Session is down");
            return false;
        }

        return true;
    }
}
