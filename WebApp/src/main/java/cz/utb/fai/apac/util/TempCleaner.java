/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import cz.utb.fai.apac.database.SubmissionService;
import cz.utb.fai.apac.entity.Submission;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author František Špaček
 */
@Singleton
@Startup
public class TempCleaner {

    private static final Logger LOGGER = LoggerFactory.getLogger(TempCleaner.class);
    private static final int DAYS = 30;

    /**
     * Delete old submissions
     * @throws SQLException
     * @throws IOException 
     */
    @Schedule(dayOfMonth = "1", persistent = false)
    public static void clean() throws SQLException, IOException {
        LOGGER.info("Loading old submissions");
        List<Submission> oldSubmissions = SubmissionService.getOlderThan(DAYS);

        for (Submission submission : oldSubmissions) {
            LOGGER.info("Deleting temp dir {}", submission.getSubmissionWorkspace().getAbsolutePath());
            FileUtils.deleteDirectory(submission.getSubmissionWorkspace());
        }

    }
}
