/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.database;

import cz.utb.fai.apac.entity.Assignment;
import cz.utb.fai.apac.util.DatabaseUtil;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class AssignmentService {

    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM assignments WHERE id = ?";
    private static final String INSERT_QUERY = "INSERT INTO assignments (language, threshold, "
            + "max_runtime, max_size, updated, name, description) VALUES(?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE assignments SET language = ?, threshold = ?, max_runtime = ?, max_size = ?, "
            + "updated = 1, timestamp = now(), name = ?, description = ? WHERE id = ?;";
    private static final String UPDATE_BASEFILE_QUERY = "UPDATE assignments SET basefile = ?, updated = 1, timestamp = now() WHERE id = ?;";
    private static final String UPDATE_STATUS_QUERY = "UPDATE assignments SET updated = ?, timestamp = now() WHERE id = ?;";
    private static final String SELECT_ALL_QUERY = "SELECT * FROM assignments WHERE timestamp > (NOW() - INTERVAL ? YEAR)";
    private static final String DELETE_QUERY = "DELETE FROM assignments WHERE id = ?";

    public static Assignment getById(long id) throws SQLException {
        Assignment assignment = null;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_BY_ID_QUERY)) {

            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                assignment = new Assignment();
                assignment.setId(result.getLong("id"));
                assignment.setLanguage(result.getString("language"));
                assignment.setMaxRuntime(result.getInt("max_runtime"));
                assignment.setThreshold(result.getInt("threshold"));
                assignment.setMaxSize(result.getInt("max_size"));
                assignment.setTimestamp(result.getTimestamp("timestamp"));
                assignment.setUpdated(result.getBoolean("updated"));
                assignment.setName(result.getString("name"));
                assignment.setDescription(result.getString("description"));

                String path = result.getString("baseFile");

                if (path != null && !path.isEmpty()) {
                    File baseFile = new File(path);
                    assignment.setBaseFile(baseFile);
                }
            }
        }
        return assignment;
    }

    public static Assignment insert(Assignment assignment) throws SQLException {
        Assignment created = null;
        int generatedId = -1;

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, assignment.getLanguage());
            statement.setInt(2, assignment.getThreshold());
            statement.setInt(3, assignment.getMaxRuntime());
            statement.setInt(4, assignment.getMaxSize());
            statement.setBoolean(5, true);
            statement.setString(6, assignment.getName());
            statement.setString(7, assignment.getDescription() == null ? "" : assignment.getDescription());

            statement.execute();
            ResultSet result = statement.getGeneratedKeys();
            if (result.next()) {
                generatedId = result.getInt(1);
            }
        }

        if (generatedId > -1) {
            created = getById(generatedId);
        }

        return created;

    }

    public static Assignment update(Assignment assignment) throws SQLException {
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_QUERY)) {

            statement.setString(1, assignment.getLanguage());
            statement.setInt(2, assignment.getThreshold());
            statement.setInt(3, assignment.getMaxRuntime());
            statement.setInt(4, assignment.getMaxSize());
            statement.setString(5, assignment.getName());
            statement.setString(6, assignment.getDescription());
            statement.setLong(7, assignment.getId());

            statement.execute();
        }

        return getById(assignment.getId());
    }

    public static void updateBaseFile(long id, String path) throws SQLException {
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_BASEFILE_QUERY)) {
            statement.setString(1, path);
            statement.setLong(2, id);
            statement.execute();

        }
    }

    public static void setUpdated(long id, boolean updated) throws SQLException {
        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(UPDATE_STATUS_QUERY)) {
            statement.setBoolean(1, updated);
            statement.setLong(2, id);
            statement.execute();
        }
    }

    public static List<Assignment> getAll(int years) throws SQLException {
        List<Assignment> assigments = new ArrayList<>();

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(SELECT_ALL_QUERY)) {
            statement.setInt(1, years);
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Assignment assignment = new Assignment();
                assignment.setId(result.getLong("id"));
                assignment.setLanguage(result.getString("language"));
                assignment.setMaxRuntime(result.getInt("max_runtime"));
                assignment.setThreshold(result.getInt("threshold"));
                assignment.setMaxSize(result.getInt("max_size"));
                assignment.setTimestamp(result.getTimestamp("timestamp"));
                assignment.setUpdated(result.getBoolean("updated"));
                assignment.setName(result.getString("name"));
                assignment.setDescription(result.getString("description"));

                assigments.add(assignment);
            }
        }

        return assigments;
    }

    public static void delete(long assignmentId) throws SQLException {

        try (Connection conn = DatabaseUtil.getConnection();
                PreparedStatement statement = conn.prepareStatement(DELETE_QUERY)) {

            statement.setLong(1, assignmentId);
            statement.execute();
        }
    }
}
