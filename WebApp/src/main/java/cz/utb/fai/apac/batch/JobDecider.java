/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.api.Decider;
import javax.batch.runtime.StepExecution;
import javax.inject.Named;

/**
 *
 * @author František Špaček
 */
@Named
public class JobDecider implements Decider {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobDecider.class);

    @Override
    public String decide(StepExecution[] executions) throws Exception {
        LOGGER.debug("Decing for next step");
        
        for (StepExecution execution : executions) {

            if (execution.getStepName().equals("process-input-step")) {
                return execution.getExitStatus();
            }

            if (execution.getStepName().equals("compilation-step")) {
                return execution.getExitStatus();
            }
        }

        return String.valueOf(ExitStatus.FAILED);
    }

}
