/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.dto.TaskDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.operations.JobOperator;
import javax.batch.operations.NoSuchJobException;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.batch.runtime.JobInstance;
import javax.validation.constraints.Min;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author František Špaček
 */
@Path("tasks")
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {

    private static final String JOB_NAME = "apacJob";
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskResource.class);
    private static final JobOperator jobOperator = BatchRuntime.getJobOperator();

    @GET
    public Response getTasks(@QueryParam("from") Integer from, @QueryParam("to") Integer to) {

        if (from == null || from < 0) {
            from = 0;
        }

        if (to == null || to < 0) {
            to = 500;
        }

        //filter swap
        if (from > to) {
            from = from ^ to;
            to = from ^ to;
            from = from ^ to;
        }

        List<TaskDTO> tasks = new ArrayList<>();

        try {
            for (JobInstance jobInstance : jobOperator.getJobInstances(JOB_NAME, from, to - from)) {

                for (JobExecution execution : jobOperator.getJobExecutions(jobInstance)) {
                    TaskDTO task = new TaskDTO();
                    task.setCreateTime(execution.getCreateTime());
                    task.setStartTime(execution.getStartTime());
                    task.setEndTime(execution.getEndTime());
                    task.setExecutionId(execution.getExecutionId());
                    task.setParameters(execution.getJobParameters());
                    task.setExitStatus(execution.getExitStatus());
                    tasks.add(task);

                }
            }
        } catch (NoSuchJobException ex) {
            LOGGER.error("No such job", ex);
        }

        Collections.sort(tasks, (TaskDTO o1, TaskDTO o2) -> o2.getExecutionId().compareTo(o1.getExecutionId()));

        return Response.ok(tasks).build();
    }

    @POST
    @Path("{taskId}/restart")
    public Response restartJob(@Min(1) @PathParam("taskId") long id) {
        JobExecution execution = jobOperator.getJobExecution(id);
        jobOperator.restart(id, execution.getJobParameters());

        return Response.ok().build();
    }

    @POST
    @Path("{taskId}/cancel")
    public Response cancelJob(@Min(1) @PathParam("taskId") long id) {
        jobOperator.abandon(id);

        return Response.ok().build();
    }
}
