/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.batch;

import cz.utb.fai.apac.database.ExecutionService;
import cz.utb.fai.apac.database.SubmissionService;
import cz.utb.fai.apac.database.VectorService;
import cz.utb.fai.apac.entity.Execution;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.processor.SubmissionProcessorFactory;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import cz.utb.fai.apac.util.FormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.api.AbstractBatchlet;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author František Špaček
 */
@Named
public class WriterStep extends AbstractBatchlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionStep.class);

    @Inject
    private JobContext jobContext;

    @Inject
    private SubmissionProcessorFactory processorFactory;

    @Override
    public String process() throws Exception {
        String exitStatus = String.valueOf(ExitStatus.RESULTS_NOT_WRITTEN);

        Submission jobData = (Submission) jobContext.getTransientUserData();

        String language = jobData.getAssignment().getLanguage();
        LOGGER.debug("Submission language is {}", language);

        //get instance of supported language
        SubmissionProcessor processor = processorFactory.getInstance(language);

        boolean referenceSubmission = Boolean
                .valueOf(BatchRuntime.getJobOperator()
                        .getParameters(jobContext.getExecutionId())
                        .getProperty("referenceSubmission"));

        jobData.setReference(referenceSubmission);

        Submission createdSubmission = SubmissionService.insert(jobData);

        //successfully created submission record in databse
        if (createdSubmission != null) {
            if (processor != null && jobData.getExecutions() != null) {
                double totalPoints = 0;
                for (Execution e : jobData.getExecutions()) {
                    if (referenceSubmission) {
                        VectorService.updateOutputById(e.getVectorId(),
                                FormatUtil.getOutputFromValgrindOutput(e.getOutput()));
                        createdSubmission.setReference(true);
                    } else {
                        
                        //calc grading for tests output
                        double points = processor.caclScore(jobData.getCompilerOutput(), e);
                        e.setPoints(points);

                        totalPoints += points;
                    }

                    e.setSubmissionId(createdSubmission.getId());
                    ExecutionService.insert(e);
                }

                createdSubmission.setTotalPoints(totalPoints);

                createdSubmission = SubmissionService.update(createdSubmission);

                if (createdSubmission != null) {
                    exitStatus = String.valueOf(ExitStatus.RESULTS_WRITTEN);
                }
            } else {
                exitStatus = String.valueOf(ExitStatus.RESULTS_WRITTEN);
            }
        }

        jobContext.setExitStatus(exitStatus);
        return exitStatus;

    }

}
