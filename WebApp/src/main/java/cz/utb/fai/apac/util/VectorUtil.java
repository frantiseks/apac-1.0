/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import cz.utb.fai.apac.dto.VectorDTO;
import cz.utb.fai.apac.entity.Argument;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.enums.ArgumentType;

import java.util.List;

/**
 *
 * @author František Špaček
 */
public class VectorUtil {

    public static boolean validate(VectorDTO vector) {
        boolean valid = vector.getPoints() > 0;
        valid &= vector.getArguments() != null;
        int stdinCounter = 0;

        if (!vector.getArguments().isEmpty() && valid) {
            for (Argument arg : vector.getArguments()) {
                if (arg.getName() == null) {
                    arg.setName("");
                }
                valid &= arg.getType() != null;
                valid &= arg.getValue() != null;

                if (valid && arg.getType().equals(ArgumentType.STDINFILE)) {
                    arg.setName("<");
                    stdinCounter++;
                }
            }
        }

        valid &= stdinCounter < 2;

        return valid;
    }

    public static boolean validateSTDINFile(List<Argument> arguments, String filename) {
        if (arguments == null) {
            return false;
        }

        for (Argument arg : arguments) {
            if (arg.getType().equals(ArgumentType.STDINFILE)) {
                return arg.getValue().equals(filename);
            }
        }

        return true;
    }

    public static String getSTDINFileName(Vector vector) {
        for (Argument arg : vector.getArguments()) {
            if (arg.getType().equals(ArgumentType.STDINFILE)) {
                return arg.getValue();
            }
        }

        return "";
    }
}
