/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.api;

import cz.utb.fai.apac.dto.PluginDTO;
import cz.utb.fai.apac.dto.StatusDTO;
import cz.utb.fai.apac.processor.SubmissionProcessorFactory;
import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.GenericExtFilter;
import cz.utb.fai.apac.util.SandboxClient;
import io.docker.DockerException;
import io.docker.DockerNotFoundException;
import io.docker.DockerNotRunningException;
import io.docker.entity.ContainerDetail;
import io.docker.entity.ContainerProcesses;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
@Path("admin")
@Produces(MediaType.APPLICATION_JSON)
public class AdminResource {

    private static final SandboxClient client = new SandboxClient();
    private static final String PS_ARGS = "u";

    @DELETE
    @POST
    @Path("container/{containerId}/delete")
    public Response deleteContainer(@NotNull @Size(min = 64, max = 64) @PathParam("containerId") String containerId)
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        client.deleteContainer(containerId);
        return Response.ok().build();
    }

    @POST
    @Path("container/{containerId}/restart")
    public Response restartContainer(@NotNull @Size(min = 64, max = 64) @PathParam("containerId") String containerId)
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        client.restartContainer(containerId);
        return Response.ok().build();
    }

    @POST
    @Path("container/{containerId}/kill")
    public Response killContainer(@NotNull @Size(min = 64, max = 64) @PathParam("containerId") String containerId)
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        client.killContainer(containerId);
        return Response.ok().build();
    }

    @POST
    @Path("container/{containerId}/stop")
    public Response stopContainer(@NotNull @Size(min = 64, max = 64) @PathParam("containerId") String containerId)
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        client.stopContainer(containerId);
        return Response.ok().build();
    }

    @GET
    @Path("container/{containerId}/")
    public Response getContainerDetail(@NotNull @Size(min = 64, max = 64) @PathParam("containerId") String containerId)
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        ContainerDetail detail = client.inspectContainer(containerId);
        return Response.ok(detail).build();
    }

    @GET
    @Path("container/{containerId}/processes")
    public Response getContainerProcesses(@NotNull @Size(min = 64, max = 64) @PathParam("containerId") String containerId)
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        ContainerProcesses processes = client.getContainerProcesses(containerId, PS_ARGS);
        return Response.ok(processes).build();

    }

    @GET
    @Path("container")
    public Response getContainers()
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        return Response.ok(client.getContainers(true)).build();
    }

    @GET
    @Path("status")
    public Response getStatus()
            throws DockerException, DockerNotFoundException, DockerNotRunningException {
        StatusDTO status = new StatusDTO();
        //get list of only running containers
        status.setContainers(client.getContainers(false));
        status.setDockerInfo(client.getInfo());
        status.setDockerVersion(client.getVersion());

        //list of loaded plugins
        status.setProcessors(SubmissionProcessorFactory.getLoadedPlugins());

        return Response.ok(status).build();

    }

    @GET
    @Path("plugin")
    public Response getPlugins() {
        File processorsDir = new File(ConfigurationUtil.getProperty("apac.pluginsDir"));
        File[] jars = processorsDir.listFiles(new GenericExtFilter(new String[]{"jar"}));

        List<PluginDTO> plugins = new ArrayList<>();
        for (File jar : jars) {
            plugins.add(new PluginDTO(jar.getName(), jar.lastModified()));
        }

        return Response.ok(plugins).build();
    }
}
