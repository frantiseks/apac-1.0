/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.utb.fai.apac.batch;

import cz.utb.fai.apac.entity.CompilerResponse;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.enums.ResponseCode;
import cz.utb.fai.apac.processor.SubmissionProcessorFactory;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.api.AbstractBatchlet;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;

/**
 *
 * @author František Špaček
 */
@Named
public class CompilationStep extends AbstractBatchlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompilationStep.class);
    
    @Inject
    private JobContext jobContext;

    @Inject
    private SubmissionProcessorFactory processorFactory;

    @Override
    public String process() throws Exception {
        String exitStatus = String.valueOf(ExitStatus.COMPILATION_FAILED);

        //get data for processing
        Submission jobData = (Submission) jobContext.getTransientUserData();
        String language = jobData.getAssignment().getLanguage();

        LOGGER.info("Submission language is {}", language);

        //get instance of supported language
        SubmissionProcessor processor = processorFactory.getInstance(language);

        if (processor != null) {

            //get workspace dir from jobContext
            File submissionWorkspace = jobData.getSubmissionWorkspace();
            CompilerResponse result = processor.compile(submissionWorkspace);
            jobData.setCompilerOutput(result.getCompilerResult());

            if (result.getCode() == ResponseCode.Compiler.BUILD_SUCCESS) {
                exitStatus = String.valueOf(ExitStatus.COMPILATION_SUCCESS);
                // jobData.setCompilerResult(result);
            }else{
                jobData.setCompileFailed(true);
            }
        } else {
            exitStatus = String.valueOf(ExitStatus.LANGUAGE_NOT_SUPPORTED);
        }

        jobContext.setExitStatus(exitStatus);
        return exitStatus;

    }

}
