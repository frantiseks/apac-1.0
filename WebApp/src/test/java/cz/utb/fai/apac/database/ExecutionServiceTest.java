/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.utb.fai.apac.database;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import cz.utb.fai.apac.util.ConfigurationUtil;
import cz.utb.fai.apac.util.DatabaseUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author František Špaček
 */
public class ExecutionServiceTest {
    
    public ExecutionServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
         ConfigurationUtil.initDebug("/home/expi/Documents/Projects/APACWildfly/src/main/webapp/WEB-INF/classes/config.properties");

        try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,
                    "org.apache.naming");
            InitialContext ic = new InitialContext();

            ic.createSubcontext("java:");
            ic.createSubcontext("java:/jdbc");

            // Construct DataSource
            MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();
            ds.setURL("jdbc:mysql://localhost:3306/apac2");
            ds.setUser("root");
            ds.setPassword("root");

            ic.bind("java:/jdbc/mysql", ds);
            DatabaseUtil.initDebug(ic);

        } catch (NamingException ex) {
            System.out.println(ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testGetAllBySubmissionId() throws Exception {
    }

    @Test
    public void testGetById() throws Exception {
    }

    @Test
    public void testInsert() throws Exception {
    }
    
}
