package cz.utb.fai.apac.util;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import cz.utb.fai.apac.entity.Argument;
import cz.utb.fai.apac.entity.Submission;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.enums.ArgumentType;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class TempCleanerTest {

    public TempCleanerTest() {
    }

    @BeforeClass
    public static void setUp() {
        ConfigurationUtil.initDebug("/home/expi/Documents/Projects/APACWildfly/src/main/webapp/WEB-INF/classes/config.properties");

        // rcarver - setup the jndi context and the datasource
        try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,
                    "org.apache.naming");
            InitialContext ic = new InitialContext();

            ic.createSubcontext("java:");
            ic.createSubcontext("java:/jdbc");

            // Construct DataSource
            MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();
            ds.setURL("jdbc:mysql://localhost:3306/apac");
            ds.setUser("root");
            ds.setPassword("root");

            ic.bind("java:/jdbc/mysql", ds);
            DatabaseUtil.initDebug(ic);

        } catch (NamingException ex) {
            System.out.println(ex);
        }

    }

    @AfterClass
    public static void cleanUp() {

    }

    @Test
    public void testClean() throws Exception {
        System.out.println(UUIDUtil.random());
    }
    
    @Test
    public void testJson(){
        Vector v = new Vector();
        v.setOutput("test");
        v.setPoints(10);
        
        List<Argument> arguments =new ArrayList<>();
        
        Argument argument = new Argument();
        argument.setName("test");
        argument.setType(ArgumentType.STRING);
        argument.setValue("5");
        arguments.add(argument);
        
        v.setArguments(arguments);
        
        List<Vector> vectors = new ArrayList<>();
        vectors.add(v);
        
        System.out.println(FormatUtil.json(vectors));
    }
    
    @Test
    public void testJson2(){
        Submission s = new Submission();
        System.out.println(FormatUtil.json(s));
    }

}
