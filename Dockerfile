FROM debian:jessie
MAINTAINER Frantisek Spacek <spacek@fai.utb.cz>

# make sure the package repository is up to date
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
RUN apt-get update

# automatically accept oracle license
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections

# and install java 8 oracle jdk
RUN apt-get -y install oracle-java8-installer openssh-server valgrind whois --no-install-recommends && apt-get clean
RUN update-alternatives --display java && mkdir /var/run/sshd 

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
RUN echo 'root:sandbox'|chpasswd && useradd sandbox --create-home -m -p `mkpasswd sandbox`

CMD /usr/sbin/sshd -D
EXPOSE 22
