package cz.utb.fai.apac.processor;

import com.google.gson.Gson;
import cz.utb.fai.apac.entity.CompilerResponse;
import cz.utb.fai.apac.entity.Vector;
import cz.utb.fai.apac.enums.ResponseCode;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import java.io.File;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author František Špaček
 */
public class CSubmissionProcessorTest {

    private final SubmissionProcessor cProcessor = new CSubmissionProcessor();
    private static final String SUBMISSION_TEMP = "/home/expi/apac/temp/098203a223a24235bba3f6f06aca138a";
    private static final String JSON_TEST_VECTOR = "{\"args\":[{\"name\":\"\",\"type\":\"FILE\",\"value\":\" "
            + "/*obsnwukiwkvgm\\nixofxumknsafgzwtrfwyoxxygdihrszqccqfnfsg/v*w*xd*/\\nioihqoek /*cnqgcbbs\\nqvwx"
            + "fnxnsxapnlecv*j/qsbh/lzot**/\\nrqwdrsyysqelqjkmepdzplneslrrrpvainyxhqc /*omqqvchlwcljmnphf\\nbkj"
            + "dhhexcvecpei/nrzj/spg/t*kw*nplv*g/ppu*v*xqfckqj/sv*v*t*v*h/*/\\nwsnqxryoxawxpdwbtsmziemufyzrjhco"
            + " /*ylicgjhanffwnrykh\\nprwnhwgihbjlcpzov*d*/\\nreamdtouwrhmxhxsomblpyrv /*czkxigtbrx\\ndxyrcoci/"
            + "u*oev*g/yozi/y*/\\nukvkqtjlzinwkedqurcwiizdczqx /*i\\neqpfkemxdvwtnkkyqynwtst*ynoi/scmoj/j/bbseq"
            + "i/ng/*/\\nnkktdj /*apfrvgvngnhvgelbhxkbocr\\nffqqwbycyxdkg/xprv*romyi/*/\\ntnrlvajnybxnlndrnsghu"
            + "krqhiigd /*veksnupxrfyuokytzmoybg\\nbkcmg/yg/oi/t*i/bsv*v*g/u*arxu**/\\nsxtjieshqyqwdzeohgunkfne"
            + "lsqldpqpbzyv /*fkcbqjknhnlxwzs\"},{\"name\":\"\",\"type\":\"OUTPUT\",\"value\":\"output\"}],\"ou"
            + "tput\":\" \\nioihqoek \\nwsnqxryoxawxpdwbtsmziemufyzrjhco \\nreamdtouwrhmxhxsomblpyrv \\nukvkqtj"
            + "lzinwkedqurcwiizdczqx \\nnkktdj \\ntnrlvajnybxnlndrnsghukrqhiigd \\n\",\"points\":5}";

    @Test
    @Ignore
    public void testCompile() {
        File assignmentTempDir = new File(SUBMISSION_TEMP);

        if (assignmentTempDir.exists()) {
            CompilerResponse result = cProcessor.compile(assignmentTempDir);
            Assert.assertEquals(ResponseCode.Compiler.BUILD_SUCCESS, result.getCode());
        }

    }

    @Test
    public void testCaclScore() {
    }

}
