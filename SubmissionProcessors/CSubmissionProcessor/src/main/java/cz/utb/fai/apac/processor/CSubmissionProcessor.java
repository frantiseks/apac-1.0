package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.entity.CompilerResponse;
import cz.utb.fai.apac.entity.Execution;
import cz.utb.fai.apac.enums.Extension;
import cz.utb.fai.apac.enums.ResponseCode;
import cz.utb.fai.apac.spi.ProcessorInfo;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import cz.utb.fai.apac.util.DirFilter;
import cz.utb.fai.apac.util.FileUtil;
import cz.utb.fai.apac.util.FormatUtil;
import cz.utb.fai.apac.util.GenericExtFilter;
import cz.utb.fai.apac.util.ProcessUtil;
import cz.utb.fai.apac.util.ScoreUtil;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author František Špaček
 */
@ProcessorInfo(language = "C",version = 1.0)
public class CSubmissionProcessor implements SubmissionProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CSubmissionProcessor.class);
    private static final String COMPILER_C_COMMAND = "gcc -Wall -std=c99";
    private static final String COMPILER_C_OPTIONAL_ARGS = "-fopenmp";
    private static final String VALGRIND_COMMAND = "valgrind --leak-check=full ./%s";
    private static final int ALLOWED_COMPILER_WARNINGS = 2;
    private static final GenericExtFilter SOURCE_FILES_FILTER = new GenericExtFilter(new String[]{"c", "h"});

    private static final double W1 = 0.5;
    private static final double W2 = 0.4;
    private static final double W3 = 0.1;

    @Override
    public CompilerResponse compile(File assignmentWorkspace) {
        CompilerResponse result = new CompilerResponse(ResponseCode.Compiler.BUILD_FAILED);

        File[] sourcesDirs = assignmentWorkspace.listFiles(new DirFilter());

        if (sourcesDirs.length > 0) {
            try {
                FileUtil.getFilesFromDirs(assignmentWorkspace, sourcesDirs, SOURCE_FILES_FILTER);
            } catch (IOException ex) {
                LOGGER.error("Copy files from dir exception", ex);
                return result;
            }
        }
        Set<String> sourceFiles = new HashSet<>(Arrays.asList(assignmentWorkspace.list(SOURCE_FILES_FILTER)));

        String command = COMPILER_C_COMMAND;
        String outputFile = String.format("%s%s%s.%s", assignmentWorkspace.getAbsolutePath(), File.separator, assignmentWorkspace.getName(), Extension.BUILD.toString());

        command += String.format(" %s -o %s %s", FileUtil.buildSourcesList(sourceFiles, assignmentWorkspace.getAbsolutePath()), outputFile, COMPILER_C_OPTIONAL_ARGS);

        try {
            String compileOutput = ProcessUtil.runAndRead(command);

            boolean success = !compileOutput.contains("error") && !compileOutput.contains("returned");
            LOGGER.info(String.format("Compile output %s", compileOutput));

            result.setCode(success ? ResponseCode.Compiler.BUILD_SUCCESS : ResponseCode.Compiler.BUILD_FAILED);
            result.setCompilerResult(compileOutput);

        } catch (IOException ex) {
            result.setCode(ResponseCode.Compiler.BUILD_FAILED);
            LOGGER.error("Compile exception for workspace {}", assignmentWorkspace.getAbsolutePath(), ex);
        }

        return result;
    }

    public String executeCommand() {
        return VALGRIND_COMMAND;
    }

    @Override
    public double caclScore(String compilerOutput, Execution execution) {
        if (StringUtils.containsIgnoreCase(compilerOutput, "error")) {
            return 0.0;
        }

        if (execution == null || execution.getOutput().isEmpty() || StringUtils.containsIgnoreCase(execution.getOutput(), "killed")) {
            return 0;
        }

        if (execution.getVector() == null) {
            return 0;
        }

        String referenceOutput = execution.getVector().getOutput();
        double maxPoints = execution.getVector().getPoints();

        String cleanOutput = FormatUtil.getOutputFromValgrindOutput(execution.getOutput());
        double maxSimilarityPoints = W1 * maxPoints;
        double maxMemoryLeaksPoints = W2 * maxPoints;
        double maxWarningsPoints = W3 * maxPoints;

        if (referenceOutput != null && !referenceOutput.isEmpty()) {
            double similarity = ScoreUtil.calcOutputSimilarity(cleanOutput, referenceOutput);

            if (similarity < (execution.getOutputMatchTreshold() / 100.0)) {
                similarity = 0;

            }
            maxSimilarityPoints *= similarity;
        }

        if (containsMemoryLeaks(execution.getOutput())) {
            maxMemoryLeaksPoints = 0;
        }

        int warningsCount = calcCompileWarnings(compilerOutput);
        if (warningsCount > ALLOWED_COMPILER_WARNINGS) {
            maxWarningsPoints = 0;
        }

        return maxSimilarityPoints + maxMemoryLeaksPoints + maxWarningsPoints;
    }

    private static int calcCompileWarnings(String compilerResult) {
        int warnings = 0;
        Scanner scanner = new Scanner(compilerResult);
        while (scanner.hasNextLine()) {
            if (scanner.nextLine().contains("warning")) {
                warnings++;
            }
        }

        return warnings;
    }

    private static boolean containsMemoryLeaks(String valgrindOutput) {
        String definitelyLost = StringUtils.substringBetween(valgrindOutput, "definitely lost", "bytes");

        if (definitelyLost == null || definitelyLost.isEmpty()) {
            return false;
        }

        int lostBytes = Integer.valueOf(definitelyLost.replaceAll("[^0-9]", ""));
        return lostBytes > 0;
    }

    @Override
    public String executionCommand() {
        return VALGRIND_COMMAND;
    }

}
