package cz.utb.fai.apac.processor;

import cz.utb.fai.apac.entity.CompilerResponse;
import cz.utb.fai.apac.entity.Execution;
import cz.utb.fai.apac.enums.Extension;
import cz.utb.fai.apac.enums.ResponseCode;
import cz.utb.fai.apac.spi.ProcessorInfo;
import cz.utb.fai.apac.spi.SubmissionProcessor;
import cz.utb.fai.apac.util.GenericExtFilter;
import cz.utb.fai.apac.util.ProcessUtil;
import cz.utb.fai.apac.util.ScoreUtil;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author František Špaček
 */

@ProcessorInfo(language = "JAVA",version = 1.0)
public class JavaSubmissionProcessor implements SubmissionProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaSubmissionProcessor.class);
    private static final String ANT_CMD = "ant -q -buildfile %s";
    private static final String BUILD_XML_NAME = "build.xml";
    private static final String JAVA_EXEC_CMD = "java -jar %s";

    @Override
    public CompilerResponse compile(File assignmentWorkspace) {
        CompilerResponse result = new CompilerResponse(ResponseCode.Compiler.BUILD_FAILED);

        List<File> allFiles = new ArrayList<>(FileUtils.listFiles(assignmentWorkspace, new RegexFileFilter("^(.*?)"), DirectoryFileFilter.DIRECTORY));

        String buildXMLPath = "";
        File projectDir = null;
        for (File file : allFiles) {
            if (file.getName().equals(BUILD_XML_NAME)) {
                buildXMLPath = file.getAbsolutePath();
                projectDir = file.getParentFile();
                break;
            }
        }

        if (buildXMLPath.isEmpty() || projectDir == null) {
            result.setCode(ResponseCode.Compiler.NOT_SUPPORTED);
        } else {
            try {
                String compileOutput = ProcessUtil.runAndRead(String.format(ANT_CMD, buildXMLPath));

                if (compileOutput.contains("SUCCESSFUL")) {

                    FilenameFilter jarFilter = new GenericExtFilter(new String[]{".jar"});
                    File distFolder = new File(String.format("%s%s%s", projectDir.getAbsolutePath(), File.separator, "dist"));

                    String buildOutputPath = String.format("%s%s%s.%s", assignmentWorkspace, File.separator, assignmentWorkspace.getName(), Extension.BUILD);
                    if (distFolder.isDirectory() && distFolder.list(jarFilter).length == 1) {
                        File outputJar = new File(buildOutputPath);
                        FileUtils.copyFile(distFolder.listFiles(jarFilter)[0], outputJar);
                    }

                    result.setCode(ResponseCode.Compiler.BUILD_SUCCESS);
                }

                result.setCompilerResult(compileOutput);

            } catch (IOException ex) {
                result.setCode(ResponseCode.Compiler.BUILD_FAILED);
                LOGGER.error("Compile exception for workspace {}", assignmentWorkspace.getAbsolutePath(), ex);
            }
        }
        return result;
    }

    @Override
    public String executionCommand() {
        return JAVA_EXEC_CMD;
    }

    @Override
    public double caclScore(String compileOutput, Execution execution) {
        if (execution.isTerminated() || execution.getOutput() == null || execution.getOutput().isEmpty()) {
            return 0;
        }

        String referenceOutput = execution.getVector().getOutput();
        double maxPoints = execution.getVector().getPoints();

        double w1 = 0.9;
        double w2 = 0.1;

        double maxSimilarityPoints = w1 * maxPoints;
        double maxWarningsPoints = w2 * maxPoints;

        if (referenceOutput != null && !referenceOutput.isEmpty()) {
            double similarity = ScoreUtil.calcOutputSimilarity(execution.getOutput(), referenceOutput);
            LOGGER.info(String.format("Similarity is %s", similarity));

            if (similarity < (execution.getOutputMatchTreshold() / 100.0)) {
                similarity = 0;

            }
            maxSimilarityPoints *= similarity;
        }

        return maxSimilarityPoints + maxWarningsPoints;
    }

}
