/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.entity;

import java.util.Date;

/**
 *
 * @author František Špaček
 */
public class Execution {
    private long id;
    private long vectorId;
    private long submissionId;
    private double points;
    private String output;
    private Date timestamp;
    private boolean terminated;
    private int code;
    private Vector vector;
    private int outputMatchTreshold;

    public Execution() {
    }

    public Execution(int code) {
        this.code = code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(long submissionId) {
        this.submissionId = submissionId;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Vector getVector() {
        return vector;
    }

    public void setVector(Vector vector) {
        this.vector = vector;
    }

    public long getVectorId() {
        return vectorId;
    }

    public void setVectorId(long vectorId) {
        this.vectorId = vectorId;
    }

    public int getOutputMatchTreshold() {
        return outputMatchTreshold;
    }

    public void setOutputMatchTreshold(int outputMatchTreshold) {
        this.outputMatchTreshold = outputMatchTreshold;
    }
    
    @Override
    public String toString() {
        return "Execution{" + "id=" + id + ", submissionId=" + submissionId + ", points=" + points + ", output=" + output + ", timestamp=" + timestamp + ", terminated=" + terminated + '}';
    }
    
}
