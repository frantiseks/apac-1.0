/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author František Špaček
 */
public class ProcessUtil {

    /**
     * Run and read command in current thread
     * @param command
     * @return
     * @throws IOException 
     */
    public static String runAndRead(String command) throws IOException {
        Runtime runtime = Runtime.getRuntime();
        Process compilerProcess = runtime.exec(command);
        BufferedReader outStream = new BufferedReader(new InputStreamReader(compilerProcess.getInputStream()));
        BufferedReader errStream = new BufferedReader(new InputStreamReader(compilerProcess.getErrorStream()));

        String compileOutput = IOUtils.toString(outStream);

        if (compileOutput.isEmpty()) {
            compileOutput = IOUtils.toString(errStream);
        }

        IOUtils.closeQuietly(outStream);
        IOUtils.closeQuietly(errStream);
        
        return compileOutput;
    }

}
