/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.spi;

import cz.utb.fai.apac.entity.CompilerResponse;
import cz.utb.fai.apac.entity.Execution;
import java.io.File;

/**
 *
 * @author František Špaček
 */
public interface SubmissionProcessor {
    /**
     * Compile process for files in workspace
     *
     * @param submissionWorkspace - submissionWorkspace
     * @return
     */
    public CompilerResponse compile(File submissionWorkspace);

    /**
     * Define command for program execution in container
     *
     * @return
     */
    public String executionCommand();

    /**
     * Define algorithm for grading test output
     *
     * @param compileOutput - compilation output
     * @param execution - execution output
     * @return
     */
    public double caclScore(String compileOutput, Execution execution);
}
