/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.enums;

/**
 *
 * @author František Špaček
 */
public final class ResponseCode {

    public final class Executor {

        public static final int OK = 200;
        public static final int FILE_NOT_FOUND = 404;
        public static final int ERROR = 500;
    }

    public final class Compiler {

        public static final int BUILD_SUCCESS = 200;
        public static final int BUILD_FAILED = 500;
        public static final int SOURCE_FILES_NOT_FOUND = 404;
        public static final int NOT_SUPPORTED = 405;
        public static final int UNKNOWN_ERROR = 500;
        public static final int EXCEPTION = 500;

    }

}
