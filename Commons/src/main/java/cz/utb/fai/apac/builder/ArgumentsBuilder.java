/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.builder;

import cz.utb.fai.apac.entity.Argument;
import cz.utb.fai.apac.util.UUIDUtil;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author František Špaček
 */
public class ArgumentsBuilder {

    /**
     * build argument list 
     * @param args
     * @param inputFileDirPath
     * @return
     * @throws IOException 
     */
    public static String build(List<Argument> args, String inputFileDirPath) throws IOException {

        //sorting to make sure that stdin is last
        Collections.sort(args, new Comparator<Argument>() {

            @Override
            public int compare(Argument o1, Argument o2) {
                return o1.getType().compareTo(o2.getType());
            }
        });

        StringBuilder sb = new StringBuilder();

        for (Argument arg : args) {
            if (!arg.getName().isEmpty()) {
                sb.append(arg.getName());
                sb.append(" ");
            }

            switch (arg.getType()) {
                case INPUTFILE:
                    File inputFile = new File(getInputFileName(inputFileDirPath));
                    if (inputFile.createNewFile()) {
                        FileUtils.writeStringToFile(inputFile, arg.getValue(), "UTF-8", false);

                        sb.append(inputFile.getName());
                        sb.append(" ");
                    }
                    break;
                case STRING:
                case STDINFILE:
                    if (!arg.getValue().isEmpty()) {
                        sb.append(arg.getValue());
                        sb.append(" ");
                    }
                    break;
                case OUTPUTFILE:
                    if (!arg.getValue().isEmpty()) {
                        sb.append(arg.getValue());
                        sb.append(".out");
                        sb.append(" ");
                    }
                    break;
            }
        }

        return sb.toString();
    }

    private static String getInputFileName(String path) {
        return String.format("%s%s%s.in", path, File.separator, UUIDUtil.random());
    }
}
