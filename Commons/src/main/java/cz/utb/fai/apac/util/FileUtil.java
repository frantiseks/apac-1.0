/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author František Špaček
 */
public class FileUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    /**
     * flatten submission hierarchy
     *
     * @param parentDir - parent dir
     * @param dirs - dirs to flatten
     * @param filter - extension filter
     * @throws IOException
     */
    public static void getFilesFromDirs(File parentDir, File[] dirs, GenericExtFilter filter) throws IOException {
        for (File dir : dirs) {
            LOGGER.info("Visiting dir: " + dir.getAbsolutePath());
            File[] files = dir.listFiles(filter);

            for (File file : files) {

                if (!file.isDirectory()) {
                    LOGGER.info("Copying file: " + file.getAbsolutePath());
                    FileUtils.copyFileToDirectory(file, parentDir);
                }
            }

            LOGGER.info("Deleting dir: " + dir.getAbsolutePath());
            FileUtils.deleteDirectory(dir);
        }
    }

    /**
     * build source list command
     *
     * @param sourceFiles - set of sources
     * @param workspacePath - absolute workspace path
     * @return
     */
    public static String buildSourcesList(Set<String> sourceFiles, String workspacePath) {
        StringBuilder sourcesList = new StringBuilder();
        for (String source : sourceFiles) {
            sourcesList.append(workspacePath);
            sourcesList.append(File.separator);
            sourcesList.append(source);
            sourcesList.append(" ");
        }

        return sourcesList.toString();
    }
}
