/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.entity;

import cz.utb.fai.apac.util.UUIDUtil;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 *
 * @author František Špaček
 */
public class Submission {

    private Long id;
    private String workspaceUUID;
    private Date timestamp;
    private Double totalPoints;
    private Assignment assignment;
    private File submissionWorkspace;
    private String compilerOutput;
    private Boolean compileFailed;
    private List<Execution> executions;
    private Long assignmentId;
    private Boolean reference;

    public Submission() {
        this.compileFailed = null;
        this.reference = null;
        this.workspaceUUID = UUIDUtil.random();
    }

    public Submission(String workspaceUUID) {
        this.workspaceUUID = workspaceUUID;
    }

    public String getWorkspaceUUID() {
        return workspaceUUID;
    }

    public void setWorkspaceUUID(String workspaceUUID) {
        this.workspaceUUID = workspaceUUID;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public File getSubmissionWorkspace() {
        return submissionWorkspace;
    }

    public void setSubmissionWorkspace(File submissionWorkspace) {
        this.submissionWorkspace = submissionWorkspace;
    }

    public String getCompilerOutput() {
        return compilerOutput;
    }

    public void setCompilerOutput(String compilerOutput) {
        this.compilerOutput = compilerOutput;
    }

    public List<Execution> getExecutions() {
        return executions;
    }

    public void setExecutions(List<Execution> executions) {
        this.executions = executions;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Boolean isCompileFailed() {
        return compileFailed;
    }

    public void setCompileFailed(Boolean compileFailed) {
        this.compileFailed = compileFailed;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Boolean isReference() {
        return reference;
    }

    public void setReference(Boolean reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
        return "Submission{" + "id=" + id + ", workspaceUUID=" + workspaceUUID + ", timestamp=" + timestamp
                + ", totalPoints=" + totalPoints + ", assignment=" + assignment + ", submissionWorkspace=" + submissionWorkspace
                + ", compilerOutput=" + compilerOutput + ", compileFailed=" + compileFailed + ", executions=" + executions
                + ", assignmentId=" + assignmentId + ", reference=" + reference + '}';
    }

}
