/*
 * Copyright (C) 2014 František Špaček, FAI UTB Zlin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.utb.fai.apac.util;

import com.google.gson.Gson;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author František Špaček
 */
public class FormatUtil {

    public static String json(Object object) {
        return new Gson().toJson(object);
    }

    public static <T> T fromJson(String jsonInput, Class<T> className) {
        return new Gson().fromJson(jsonInput, className);
    }

    public static <T> T fromJson(String jsonInput, Type typeOff) {
        return new Gson().fromJson(jsonInput, typeOff);
    }

    public static <T> T fromJson(Reader jsonInput, Type typeOff) {
        return new Gson().fromJson(jsonInput, typeOff);
    }

    public static void json(Object object, Type jsonType, OutputStreamWriter writer) {
        new Gson().toJson(object, jsonType, writer);
    }

    /**
     * get STDOUT from valgrind output
     *
     * @param valgrindOutput
     * @return
     */
    public static String getOutputFromValgrindOutput(String valgrindOutput) {
        StringBuilder programOutput = new StringBuilder();

        Scanner scanner = new Scanner(valgrindOutput);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!line.startsWith("==")) {
                programOutput.append(line.replaceFirst("[=]{2}[0-9]{0,4}[=]{2}", "").trim());
                programOutput.append(System.getProperty("line.separator"));
            }
        }
        return StringUtils.deleteWhitespace(programOutput.toString());
    }
}
